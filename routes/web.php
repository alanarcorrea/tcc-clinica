<?php
use Illuminate\Support\Facades\View;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::resource('', 'DashboardController');

Route::get('agenda/consulta', 'ScheduleController@agendaConsultaEsp');
Route::get('agenda/consulta/{id}/profissional', 'ScheduleController@agendaConsultaProf');
Route::get('agenda/consulta/{id}/profissional/{idEmployee}/data', 'ScheduleController@agendaConsultaData');

Route::get('agenda/procedimento', 'ScheduleController@agendaProcedimentoProc');
Route::get('agenda/procedimento/{id}/profissional', 'ScheduleController@agendaProcedimentoProf');

Route::get('agenda/{id}', 'ScheduleController@getSchedule');
Route::resource('agenda', 'ScheduleController');

Route::resource('dashboard', 'DashboardController');
Route::resource('clinica', 'ClinicController');
Route::resource('especialidades', 'SpecialityController');
Route::resource('convenios', 'AgreementController');
Route::resource('funcionarios', 'EmployeeController');

Route::get('historico', 'PacientController@history')->name('pacientes.history');
Route::resource('pacientes', 'PacientController');

Route::resource('planos', 'PlanController');
Route::resource('categorias', 'CategoriesController');
Route::resource('despesas', 'Bills_paymentController');
Route::resource('receitas', 'Bills_receiptController');
Route::resource('procedimentos', 'ClinicalProcedureController');

Route::get('atendimentos/{id}', 'CallController@index')->name('atendimentos.index');
Route::post('atendimentos', 'CallController@store')->name("atendimentos.store");
// Route::resource('atendimentos', 'CallController');

//Route::resource('relatorios', 'ReportController');

Route::get('/ajuda', function() {
  $user = Auth::user();
  View::share('user', $user);
  return view('help');
});

$this->get('/relatorios', 'ReportController@index');
$this->get('/relatorios/reportCallsMonth', 'ReportController@reportCallsMonth');
$this->get('/relatorios/reportPacients', 'ReportController@reportPacients');
$this->get('/relatorios/reportAgreements', 'ReportController@reportAgreements');
$this->get('/relatorios/reportPlans', 'ReportController@reportPlans');
$this->get('/relatorios/reportEmployees', 'ReportController@reportEmployees');
$this->get('/relatorios/reportBillsReceiptsCategories', 'ReportController@reportBillsReceiptsCategories');
$this->get('/relatorios/reportCallsMonthAgreement', 'ReportController@reportCallsMonthAgreement');
$this->get('/relatorios/reportCallsMonthEmployee', 'ReportController@reportCallsMonthEmployee');
$this->get('/relatorios/reportCallsMonthPacient', 'ReportController@reportCallsMonthPacient');
$this->get('/relatorios/reportBillsPaymentsCategories', 'ReportController@reportBillsPaymentsCategories');


$this->get('/documentos', 'DocumentController@index');
$this->get('/documentos/documentHemograma', 'DocumentController@documentHemograma');
$this->get('/documentos/documentUrianalise', 'DocumentController@documentUrianalise');
$this->get('/documentos/documentAtestado', 'DocumentController@documentAtestado');

//Route::get('pacientes2', function () {
  //  return view('paciente_perfil');
//});

//Route::get('planos2', function () {
  //  return view('planos_form');
//});
