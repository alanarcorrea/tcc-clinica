<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bills_receipt extends Model
{
    protected $fillable = array('title','value','recipient', 'payDay', 'dueDate', 'interest', 'amount', 'ourNumber', 'description', 'id_categories', 'id_employees', 'id_calls');
}
