<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacient extends Model
{
    protected $fillable = array('recordDate', 'id_agreements', 'id_users', 'id_plans');
}
