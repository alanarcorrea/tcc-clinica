<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $fillable = array('start', 'end', 'summary', 'anamnesis', 'height', 'weight', 'pressure', 'temperature', 'diagnosticHypothesis', 'prescriptionMedication', 'prescriptionExam', 'attachment', 'date', 'valueConsultation', 'valueClinicalProcedure', 'clinicalProcedure', 'note', 'id_patients', 'id_employees', 'total_value', 'id_pacients');
}
