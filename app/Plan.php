<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = array('name', 'discountConsultation', 'discountProcedure', 'id_agreements');
}
