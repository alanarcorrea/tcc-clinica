<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $fillable = array('socialName', 'fantasyName', 'cnpj', 'bank', 'agency', 'bankAccount', 'id_users');
}
