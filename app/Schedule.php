<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = array('date', 'id_pacients', 'id_employees', 'id_specialities', 'hour', 'pacient');
}
