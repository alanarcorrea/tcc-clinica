<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = array('ctps', 'salary', 'cnh', 'bank', 'agency', 'bankAccount', 'crm', 'id_functions', 'id_users', 'specialty1', 'specialty2', 'specialty3');
}
