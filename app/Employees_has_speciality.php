<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees_has_speciality extends Model
{
    protected $fillable = array('id_employees', 'id_specialities');
}
