<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicalProcedure extends Model
{
    protected $fillable = array('id_specialities', 'title', 'value', 'note');
}
