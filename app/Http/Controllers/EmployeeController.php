<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Employee;
use App\Functions;
use App\Speciality;
use App\User;
use Illuminate\Support\Facades\View;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }   
        
        $user = Auth::user();
        View::share('user', $user);

        $funcionarios= Employee::join('users',  'users.id', '=', 'employees.id_users')
        ->get();

        return view('employees', compact('funcionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        
        $functions = Functions::orderBy('name')->get();
        $specialities = Speciality::orderBy('name')->get();
        return view('employees_form', compact('action', 'functions', 'specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $storeUser = User::create(array_merge(
            $data,
            [
                'type' => 2,
                'password' => bcrypt($request->password)
            ]
        ));

        if ($storeUser) {
            $storeEmployee = Employee::create(
                array_merge(
                    $data,
                    [
                        'id_users' => $storeUser->id,
                        'ctps' => $request->ctps,
                        'salary' => $request->salary,
                        'cnh' => $request->cnh,
                        'bank' => $request->bank,
                        'agency' => $request->agency,
                        'bankAccount' => $request->bankAccount,
                        'crm' => $request->crm,
                        'cnh' => '909090',
                        'id_functions' => $request->id_functions
                    ]
                )
            );

            if ($storeEmployee) {
                return redirect()->route('funcionarios.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);
        
        $action = 2;
        
        //$reg = User::join('employees', 'employees.id_users', '=', 'users.id')
        //->get();

        $reg = DB::select("select employees.id AS idEmp, users.*, employees.* from employees inner join users where employees.id_users = users.id and users.id = $id;")[0];

        //return ['reg' => $reg];

        $functions = Functions::orderBy('name')->get();
        $specialities = Speciality::orderBy('name')->get();

        return view('employees_form', compact('action', 'reg', 'functions', 'specialities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::find($request->id_users);
        $storeUser = $user->update($data);

        //return $data;

        if ($storeUser) {
            $employee = Employee::find($id);
            $storeEmployee = $employee->update(
                array_merge(
                    $data,
                    [
                        'id_users' => $request->id_users,
                        'ctps' => $request->ctps,
                        'salary' => $request->salary,
                        'cnh' => $request->cnh,
                        'bank' => $request->bank,
                        'agency' => $request->agency,
                        'bankAccount' => $request->bankAccount,
                        'crm' => $request->crm,
                        'cnh' => '909090',
                        'id_functions' => $request->id_functions
                    ]
                )
            );

            if ($storeEmployee) {
                return redirect()->route('funcionarios.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idEmp, Request $request)
    {
        //return ['asd' => $id];

        $employee = Employee::find($idEmp);
        $user = User::find($request->id_users);

        //return $idEmp;

         if ($employee->delete() && $user->delete()) {
             return redirect()->route('funcionarios.index');
         }
    }
}
