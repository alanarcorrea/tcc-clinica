<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Schedule;
use App\Call;
use App\ClinicalProcedure;
use App\Employee;
use App\User;
use App\Speciality;
use App\Bills_receipt;
use Illuminate\Support\Facades\View;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $schedule = Schedule::find($id);

        $procedures = ClinicalProcedure::all();
        
        $employee_user_id = Employee::find($schedule->id_employees)->id_users;
        $employee_name = User::find($employee_user_id)->name;

        $discount = DB::select("select plans.discountConsultation as desconto from pacients inner join plans on pacients.id_agreements = plans.id_agreements and pacients.id = $schedule->id_pacients;")[0]->desconto;        

        $speciality_name = Speciality::find($schedule->id_specialities)->name;

        return view('calls_form', compact('schedule', 'procedures', 'employee_name', 'speciality_name', 'action', 'id', 'discount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $schedule_id = $request->id;

        $store = Call::create(
            array_merge(
                $data,
                [
                    'date' => date('Y-m-d', strtotime($request->date)),
                    'clinicalProcedure' => $request->id_clinical_procedures
                ]
            )
        );

        $newBill = Bills_receipt::create([
            'title' => 'Recebimento de consulta',
            'value' => $request->total_value,
            'payDay' => date('Y-m-d', strtotime($request->date)),
            'description' => 'Recebimento de consulta',
            'id_categories' => 6,
            'id_employees' => $request->id_employees,
            'id_calls' => $store->id
        ]);

        $removeSchedule = Schedule::find($schedule_id)->delete();

        if ($store && $newBill && $removeSchedule) {
            return redirect()->route('agenda.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
