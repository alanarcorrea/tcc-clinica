<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Agreement;
use Illuminate\Support\Facades\View;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $x = Agreement::select('COUNT(SELECT * FROM clinics)');
        if($x )

        $agreements = Agreement::orderBy('id')->get();
        return view('agreements', compact('agreements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }  
        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $agreements = Agreement::orderBy('name')->get();
        
        return view('agreements_form', compact('agreements', 'action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $store = Agreement::create($data);

        if ($store) {
            return redirect()->route('convenios.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }
        $user = Auth::user();
        View::share('user', $user);

        $action = 2;
        $agreements = Agreement::orderBy('name')->get();
        $reg = Agreement::find($id);

        return view('agreements_form', compact('action', 'reg', 'agreements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $reg = Agreement::find($id);

        $update = $reg->update($data);

        if ($update) {
            return redirect()->route('convenios.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Agreement::find($id);
        
        if ($item->delete()) {
            return redirect()->route('convenios.index');
        }
    }
}