<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MaddHatter\LaravelFullcalendar\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Schedule;
use App\Speciality;
use App\Pacient;
use App\Employee;
use App\ClinicalProcedure;
use Illuminate\Support\Facades\View;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);
        
        $schedules = Schedule::all();
        $events = [];

        // return $schedules;

        foreach($schedules as $s) {
            array_push(
                $events,
                \Calendar::event(
                    "$s->pacient ($s->id)",
                    false,
                    $s->date . "T" . $s->hour,
                    $s->date . "T" . $s->hour,
                    0
                )
            );
        }

        $calendar = \Calendar::addEvents($events)
            ->setOptions([
                'firstDay' => 1
            ])->setCallbacks([
                //'viewRender' => 'function() {alert("Callbacks!");}'
            ]);

        return view('schedules', compact('calendar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }
        
        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        
        return view('schedule_form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $specialty = $request->specialty;
        $id_specialty = Speciality::where('name', '=', $specialty)->first()->id;

        $employee = $request->employee;
        $id_employee_user_id = $request->user_id_employee;
        $id_employee = DB::select("select id from employees where id_users = $id_employee_user_id;")[0]->id;
        $pacientName = DB::select("select users.name from users inner join pacients on users.id = pacients.id_users and pacients.id = $request->id_pacients;")[0]->name;

        $storeSchedule = Schedule::create(
            array_merge(
                $data,
                [
                    'id_employees' => $id_employee,
                    'id_specialities' => $id_specialty,
                    'pacient' => $pacientName
                ]
            )
        );

        if ($storeSchedule) {
            return Redirect::action('ScheduleController@index');
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        
        if ($schedule->delete()) {
            return redirect()->route('agenda.index');
        }
    }

    public function agendaConsultaEsp() {

        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $specialities = Speciality::orderBy('name')->get();

        return view('schedule_consultation1_form', compact('specialities', 'action'));
    }

    public function agendaConsultaProf($id) {

        $user = Auth::user();
        View::share('user', $user);

        $employees = DB::select
        (
        "SELECT users.name AS profissional, users.id AS idEmployee, specialities.name AS especialidade FROM employees
        INNER JOIN users ON employees.id_users = users.id
        INNER JOIN specialities ON (employees.specialty1=specialities.id OR employees.specialty2= specialities.id OR employees.specialty3=specialities.id) AND specialities.id=$id;"
        );
        $action = 1;
        $specialty = Speciality::find($id);

        return view('schedule_consultation2_form', compact('employees', 'specialty', 'action'));
    }

    public function agendaConsultaData($id, $idEmployee){

        $user = Auth::user();
        View::share('user', $user);

        $employee = DB::select("SELECT employees.id AS id, users.name AS name, employees.* FROM employees INNER JOIN users WHERE employees.id_users = users.id AND users.id = $idEmployee;")[0];

        $specialty = Speciality::find($id); 

        //$pacients= Pacient::join('users',  'users.id', '=', 'pacients.id_users')
        //->get();

        $pacients = DB::select('select users.name, pacients.id as id from users inner join pacients on users.id = pacients.id_users;');

        $action = 1;
        return view('schedule_consultation3_form', compact('pacients', 'action', 'specialty', 'employee', 'idEmployee'));
    }

    public function getSchedule($id) {

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $schedule = Schedule::find($id);
        $specialty = DB::select ("SELECT specialities.name FROM specialities WHERE specialities.id = $schedule->id_specialities;")[0]->name;
        $employee = DB::select ("SELECT users.name FROM users INNER JOIN employees ON users.id = employees.id_users AND employees.id = $schedule->id_employees;")[0]->name;

        $schedule_id = $id;

        return view('schedule_item', compact('schedule', 'specialty', 'action', 'employee', 'schedule_id'));
    }

    public function agendaProcedimentoProc(){
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $procedures = ClinicalProcedure::orderBy('title')->get();

        return view('schedule_procedure1_form', compact('procedures', 'action'));
    
    }

    public function agendaProcedimentoProf($id){

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;

        $specialities_name = DB::select("SELECT clinical_procedures.id, clinical_procedures.id_specialities, specialities.name FROM clinical_procedures INNER JOIN specialities ON clinical_procedures.id_specialities = specialities.id AND clinical_procedures.id = $id;")[0]->name;

        $employees = DB::select("SELECT users.name AS profissional, users.id AS idUser, specialities.name AS especialidade FROM employees INNER JOIN users ON employees.id_users = users.id INNER JOIN specialities ON (employees.specialty1=specialities.id OR employees.specialty2= specialities.id OR employees.specialty3=specialities.id) AND specialities.name='$specialities_name';");


        return view('schedule_procedure2_form', compact('employees', 'action'));
    }

    public function agendaProcedimentoData($id) {

        $user = Auth::user();
        View::share('user', $user);
        
        $employee = DB::select("SELECT employees.id AS id, users.name AS name, employees.* FROM employees INNER JOIN users WHERE employees.id_users = users.id AND users.id = $id;")[0];

        $procedure = ClinicalProcedure::find($id); 

        //$pacients= Pacient::join('users',  'users.id', '=', 'pacients.id_users')
        //->get();

        $pacients = DB::select('select users.name, pacients.id as id from users inner join pacients on users.id = pacients.id_users;');

        $action = 1;
        return view('schedule_consultation3_form', compact('pacients', 'action', 'specialty', 'employee', 'idEmployee'));
    }
}
