<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ClinicalProcedure;
use App\Speciality;
use Illuminate\Support\Facades\View;

class ClinicalProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $specialities = Speciality::orderBy('id')->get();
        $procedures = ClinicalProcedure::orderBy('id')->get();

        return view('clinicalProcedure', compact('specialities', 'procedures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }  

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        $specialties = Speciality::orderBy('id')->get();

        return view('clinicalProcedure_form', compact('specialties', 'action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $store = ClinicalProcedure::create($data);

        if ($store) {
            return redirect()->route('procedimentos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 2;
        $specialities = Speciality::orderBy('id')->get();
        $reg = ClinicalProcedure::find($id);

        return view('clinicalProcedure_form', compact('action', 'reg', 'specialities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $reg = ClinicalProcedure::find($id);

        $update = $reg->update($data);

        if ($update) {
            return redirect()->route('procedimentos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ClinicalProcedure::find($id);
        
        if ($item->delete()) {
            return redirect()->route('procedimentos.index');
        }
    }
}
