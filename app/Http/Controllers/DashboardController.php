<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }        
        $user = Auth::user();
        View::share('user', $user);

        if ($user->type == 3){
            return redirect('/historico');
        }

        $totalFuncionarios = DB::select("SELECT COUNT(id) AS total FROM employees;")[0];

        $totalPacientes = DB::select("SELECT COUNT(id) AS total FROM pacients;")[0];

        $totalConvenios = DB::select("SELECT COUNT(id) AS total FROM agreements;")[0];

        $totalEspecialidades = DB::select("SELECT COUNT(id) AS total FROM specialities;")[0];

        $calls = DB::select('SELECT COUNT(date) AS total, monthname(date) AS mes FROM calls GROUP BY month(date);');


        return view('dashboard', compact('totalFuncionarios', 'totalPacientes', 'totalConvenios', 'totalEspecialidades', 'calls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
