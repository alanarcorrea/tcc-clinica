<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\Processors\Processor;
use PDF;
use App\User;
use App\Pacient;
use App\Agreement;
use App\Plan;
use App\Employee;
use App\Call;

use Illuminate\Support\Facades\View;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }
        $user = Auth::user();
        View::share('user', $user);
        
        return view('reports');
    }


    public function reportPacients()
    {
        $pacients= Pacient::join('users',  'users.id', '=', 'pacients.id_users')
        ->get();

        $agreements = Agreement::orderBy('id')->get();

        $pdf = PDF::loadView('reportPatients', compact('pacients', 'agreements'));
        return $pdf->download('relatório_pacientes.pdf');
    }

    public function reportAgreements()
    {
        $agreements = Agreement::orderBy('id')->get();

        $pdf = PDF::loadView('reportAgreements', compact('agreements'));
        return $pdf->download('relatório_convênios.pdf');
    }

    public function reportPlans()
    {
        $agreements = Agreement::orderBy('id')->get();
        $plans = Plan::orderBy('id')->get();   

        $pdf = PDF::loadView('reportPlans', compact('agreements', 'plans'));
        return $pdf->download('relatório_planos.pdf');
    }

    public function reportEmployees()
    {
        $employees= Employee::join('users',  'users.id', '=', 'employees.id_users')
        ->get();

        $pdf = PDF::loadView('reportEmployees', compact('employees'));
        return $pdf->download('relatório_funcionários.pdf');
    }

    public function reportFinalcials(){

        // $rel = DB::select("SELECT SUM(value) FROM bills_payments WHERE id_categories = ")
    }

    public function reportBillsReceiptsCategories(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $bills = DB::select('SELECT SUM(bills_receipts.value) AS valortotal, monthname(payDay) AS mes,  categories.name AS categoria
        FROM bills_receipts
        INNER JOIN categories
        ON bills_receipts.id_categories = categories.id
        GROUP BY  categories.name, month(payDay);');

        $total = DB::select('SELECT SUM(bills_receipts.value) AS valortotal,  categories.name AS categoria
        FROM bills_receipts
        INNER JOIN categories
        ON bills_receipts.id_categories = categories.id
        GROUP BY  categories.name;');

        $pdf = PDF::loadView('reportBillsReceiptsCategories', compact('bills', 'total'));
        return $pdf->download('relatório_contas-receber-categorias.pdf');
    }

    public function reportBillsPaymentsCategories(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $bills = DB::select('SELECT SUM(bills_payments.value) AS valortotal, monthname(payDay) AS mes,  categories.name AS categoria
        FROM bills_payments
        INNER JOIN categories
        ON bills_payments.id_categories = categories.id
        GROUP BY  categories.name, month(payDay);');

        $total = DB::select('SELECT SUM(bills_payments.value) AS valortotal,  categories.name AS categoria
        FROM bills_payments
        INNER JOIN categories
        ON bills_payments.id_categories = categories.id
        GROUP BY  categories.name;');

        $pdf = PDF::loadView('reportBillsPaymentsCategories', compact('bills', 'total'));
        return $pdf->download('relatório_contas-pagar-categorias.pdf');
    }

    public function reportCallsMonth(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $total = DB::select('SELECT COUNT(date) AS total FROM calls;');

        $calls = DB::select('SELECT COUNT(date) AS total, monthname(date) AS mes FROM calls GROUP BY month(date);');

        $pdf = PDF::loadView('reportCallsMonth', compact('calls', 'total'));
        return $pdf->download('relatório_atendimentos-por-mes.pdf');
    }

    public function reportCallsMonthAgreement(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $calls = DB::select('SELECT COUNT(date) AS total, monthname(date) AS mes, agreements.name AS convenio FROM calls 
        INNER JOIN pacients ON calls.id_pacients = pacients.id
        INNER JOIN agreements ON agreements.id = pacients.id_agreements
        GROUP BY month(date), agreements.name;');

        $total = DB::select('SELECT COUNT(date) AS total, agreements.name AS convenio FROM calls INNER JOIN pacients ON calls.id_pacients = pacients.id
        INNER JOIN agreements ON agreements.id = pacients.id_agreements
        GROUP BY agreements.name;');

        $pdf = PDF::loadView('reportCallsMonthAgreement', compact('calls', 'total'));
        return $pdf->download('relatório_atendimentos-mes-convenio.pdf');
    }

    public function reportCallsMonthEmployee(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $calls = DB::select('SELECT COUNT(date) AS total, users.name AS funcionario, employees.crm AS crm, monthname(date) AS mes FROM calls 
        INNER JOIN employees ON employees.id = calls.id_employees
        INNER JOIN users ON users.id = employees.id_users
        GROUP BY month(date), users.name;');

        $total = DB::select('SELECT COUNT(date) AS total, users.name AS funcionario, employees.crm AS crm FROM calls 
        INNER JOIN employees ON employees.id = calls.id_employees
        INNER JOIN users ON users.id = employees.id_users
        GROUP BY users.name;');

        $pdf = PDF::loadView('reportCallsMonthEmployee', compact('calls', 'total'));
        return $pdf->download('relatório_atendimentos-mes-funcionario.pdf');
    }

    public function reportCallsMonthPacient(){

        DB::statement("SET lc_time_names = 'pt_BR'");

        $calls = DB::select('SELECT COUNT(date) AS total, users.name AS paciente, monthname(date) AS mes FROM calls 
        INNER JOIN pacients ON calls.id_pacients = pacients.id
        INNER JOIN users ON users.id = pacients.id_users
        GROUP BY month(date), users.name;');

        $total = DB::select('SELECT COUNT(date) AS total, users.name AS paciente FROM calls 
        INNER JOIN pacients ON calls.id_pacients = pacients.id
        INNER JOIN users ON users.id = pacients.id_users
        GROUP BY users.name;');

        $pdf = PDF::loadView('reportCallsMonthPacient', compact('calls', 'total'));
        return $pdf->download('relatório_atendimentos-mes-paciente.pdf');
    }
}
