<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pacient;
use App\Agreement;
use App\Plan;
use Illuminate\Support\Facades\View;

class PacientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }        

        $user = Auth::user();
        View::share('user', $user);
        //$pacients = Pacient::orderBy('id')->get();

        $pacients= Pacient::join('users',  'users.id', '=', 'pacients.id_users')
        ->get();

        //return $users;
        return view('patients', compact('pacients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        
        $plans = Plan::orderBy('name')->get();
        $agreements = Agreement::orderBy('name')->get();
        return view('patients_form', compact('action', 'agreements', 'plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $storeUser = User::create(array_merge(
            $data,
            [
                'type' => 3,
                'password' => bcrypt($request->password)
            ]
        ));

        if ($storeUser) {
            $storePacient = Pacient::create(
                array_merge(
                    $data,
                    [
                        'id_users' => $storeUser->id,
                        'id_agreements' => $request->id_agreements,
                        'id_plans' => $request->id_plans,
                        'recordDate' => date('Y-m-d')
                    ]
                )
            );

            if ($storePacient) {
                return redirect()->route('pacientes.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 2;
        
        // $reg = User::join('pacients',  'pacients.id_users', '=', 'users.id')
        // ->get()->first();

        $reg = DB::select("select pacients.id AS idPac, users.*, pacients.* from pacients inner join users where pacients.id_users = users.id and users.id = $id;")[0];

        $plans = Plan::orderBy('name')->get();
        $agreements = Agreement::orderBy('name')->get();
        return view('patients_form', compact('action', 'reg', 'plans', 'agreements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::find($request->id_users);
        $storeUser = $user->update($data);

        if ($storeUser) {
            $pacient = Pacient::find($id);
            $storePacient = $pacient->update(
                array_merge(
                    $data,
                    [
                        'id_users' => $request->id_users,
                        'id_agreements' => $request->id_agreements,
                        'id_plans' => $request->id_plans,
                        'recordDate' => date('Y-m-d')
                    ]
                )
            );

            if ($storePacient) {
                return redirect()->route('pacientes.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $pacient = Pacient::find($id);
        $user = User::find($request->id_users);

        if ($user->delete() && $pacient->delete()) {
            return redirect()->route('pacientes.index');
        }
    }

    public function history() {
        if (!Auth::check()) {
            return redirect('/login');
        }        

        $user = Auth::user();
        View::share('user', $user);

        $pacientId = DB::select("select id as pacientId from pacients where id_users = $user->id;")[0]->pacientId;

        $history = DB::select("select date as data, prescriptionMedication as prescricao, users.name as profissional from calls
        inner join employees on employees.id = calls.id_employees
        inner join users on users.id = employees.id_users
        where id_pacients = $pacientId;");

        return view('history', compact('history'));
    }
}
