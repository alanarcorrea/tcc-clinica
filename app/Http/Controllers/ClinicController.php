<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Clinic;
use App\User;
use Illuminate\Support\Facades\View;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }        

        $user = Auth::user();
        View::share('user', $user);

        $clinics = Clinic::orderBy('socialName')->get()->first();

        $users= User::join('clinics',  'clinics.id_users', '=', 'users.id')
        ->get()->first();

        return view('clinic', compact('clinics', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);

        $action = 1;
        
        return view('clinic_form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $storeUser = User::create($data);

        if ($storeUser) {
            $storeClinic = Clinic::create(
                array_merge(
                    $data,
                    [
                        'id_users' => $storeUser->id,
                        'socialName' => $storeUser->name,
                        'password' => bcrypt($storeUser->password)
                    ]
                )
            );

            if ($storeClinic) {
                return redirect()->route('clinica.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return redirect('/login');
        }

        $user = Auth::user();
        View::share('user', $user);
        
        $action = 2;
        //$reg = Clinic::find($id);

        $reg = User::join('clinics',  'clinics.id_users', '=', 'users.id')
        ->get()->first();

        $id_users = $reg->id_users;

        return view('clinic_form', compact('action', 'reg', 'id_users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data_users_id = $request->id_users;
        
        $regUser = User::find($data_users_id);
        
        $updateUser = $regUser->update($data);

        if ($updateUser) {
            $regClinic = Clinic::find($id);

            $updateClinic = $regClinic->update(
                array_merge(
                    $data,
                    [
                        'id_users' => $regUser->id,
                        'socialName' => $regUser->name,
                        'password' => bcrypt($regUser->password)
                    ]
                )
            );

            if ($updateClinic) {
                return redirect()->route('clinica.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $itemClinic = Clinic::find($id);
        $itemUser = User::find($request->id_users);

        if ($itemClinic->delete()) {
            if ($itemUser->delete()) {
                return redirect()->route('clinica.index');
            }  
        }
    }
}
