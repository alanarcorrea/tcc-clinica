@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Agenda</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Agendar Procedimento Clínico
            @else
                Editar Procedimento Clínico
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('agenda.store') }}" method="POST">
               
        @else
            <form class="form" action="{{ route('agenda.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
        
        {{ csrf_field() }}

        <h4>Selecione o profissional para realizar o procedimento: </h4>

        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                   <tr>
                    <th>Nome</th>
                    <th style="width: 80px">Avançar</th>
                  </tr>
                </thead>
                    @foreach ($employees as $e)
                      <tr>
                        <td>{{ $e->profissional }}</td>
                        <td style="text-align: center; width: 80px">
                            <a href="{{$e->idUser}}/data">
                                <i class="fa fa-arrow-right"></i>
                                <input id="invisible_id" name="invisible" type="hidden" value="{{$e->idUser}}">
                            </a>
                        </td>
                      </tr>
                    @endforeach       
                </tbody>
              </table>  
            </div>
            
    </form>

@endsection