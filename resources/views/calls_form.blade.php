@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Agenda</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Iniciar Atendimento
            @else
                Editar Atendimento
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('atendimentos.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('atendimentos.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        <input type="hidden" name="id" id="id" value="{{ $id }}"/>
        <input type="hidden" name="discount" id="discount" value="{{ $discount }}"/>

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_employees">Profissional:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="employee" name="employee" value="{{ $employee_name }}" readonly>
                <input type="number" name="id_employees" id="id_employees" value="{{ $schedule->id_employees }}" hidden/>
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="specialty">Especialidade:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="specialty" name="specialty" value="{{ $speciality_name }}" readonly>
            </div>   
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="pacient">Paciente:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="pacient" name="pacient" value="{{ $schedule->pacient }}" readonly>
                <input type="number" name="id_pacients" id="id_pacients" value="{{ $schedule->id_pacients }}" hidden/>
            </div>   
        </div> 

        
        <div class="form-group">
            <label class="control-label col-sm-4" for="date">Data da Consulta:</label>
            <div class="col-md-5">
                <input type="date" class="form-control" id="date" name="date" placeholder="Data" value="{{ $schedule->date }}" readonly>
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="hour">Hora da Consulta:</label>
            <div class="col-md-5">
                <input type="time" class="form-control" id="hour" name="hour" placeholder="Hora" value="{{ $schedule->hour }}" readonly>
            </div>   
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="anamnesis">Anamnesis:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="anamnesis" name="anamnesis" placeholder="Anamnesis" value="{{ $reg->anamnesis or old('anamnesis') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="height">Altura:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="height" name="height" placeholder="Altura" value="{{ $reg->height or old('height') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="weight">Peso:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="weight" name="weight" placeholder="Peso" value="{{ $reg->weight or old('weight') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="pressure">Pressão:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="pressure" name="pressure" placeholder="Pressão" value="{{ $reg->pressure or old('pressure') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="temperature">Temperatura:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="temperature" name="temperature" placeholder="Temperatura" value="{{ $reg->temperature or old('temperature') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="diagnosticHypothesis">Diagnóstico Hipotético:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="diagnosticHypothesis" name="diagnosticHypothesis" placeholder="Diagnóstico Hipotético" value="{{ $reg->diagnosticHypothesis or old('diagnosticHypothesis') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="prescriptionMedication">Prescrição de Medicamentos:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="prescriptionMedication" name="prescriptionMedication" placeholder="Prescrição de Medicamentos" value="{{ $reg->prescriptionMedication or old('prescriptionMedication') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="prescriptionExam">Prescrição de Exames:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="prescriptionExam" name="prescriptionExam" placeholder="Prescrição de Exames" value="{{ $reg->prescriptionExam or old('prescriptionExam') }}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="note">Observações:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="note" name="note" placeholder="Observações" value="{{ $reg->note or old('note') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_clinical_procedures">Procedimento Clínico Realizado:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_clinical_procedures" name="id_clinical_procedures">
                    <option value="0">Selecione</option>
                    @foreach ($procedures as $p)
                        <option
                            value="{{ $p->id }}" @if ((isset ($precedures) and $p->id == $p->id_clinical_procedures) or old('id_clinical_procedures') == $p->id) selected @endif
                            data-value="{{ $p->value }}">{{ $p->title }}</option>
                    @endforeach
                </select>
            </div>          
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="valueClinicalProcedure">Valor do Procedimento Clínico:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="valueClinicalProcedure" name="valueClinicalProcedure" value="{{ $reg->valueClinicalProcedure or old('valueClinicalProcedure') }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="valueConsultation">Valor da Consulta:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="valueConsultation" name="valueConsultation" value="120.00" readonly>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="total_value">Valor total:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="total_value" name="total_value" value="120.00" readonly>
            </div>
        </div>


        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Encerrar Atendimento</button>
            </div>
        </div>
    </form>



    <form style="display: inline-block;" method="POST" action="{{ route('agenda.destroy', $schedule->id ) }}">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger">Excluir</button>
    </form>
    
    {{-- <script>
        window.onload = function() {
            const procedures = document.querySelector('#id_clinical_procedures');
            const procedureValueEl = document.querySelector('#valueClinicalProcedure');
            const totalValueEl = document.querySelector('#total_value');

            procedures.addEventListener('change', ev => {
                const target = ev.target;
                const option = ev.target.options[ev.target.selectedIndex];
                const value = parseFloat(option.getAttribute('data-value'));

                procedureValueEl.value = value.toFixed(2);
                totalValueEl.value = (value + 120).toFixed(2);
            });
        }
    </script> --}}
@endsection