@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Modelo de Documentos</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-clipboard"></i> Modelo de Documentos          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                 <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Requisição de Hemograma</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/documentos/documentHemograma">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Requisição de Urianálise </td>
                  <td style="text-align: center; width: 80px">
                    <a href="/documentos/documentUrianalise">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Modelo de Atestado</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/documentos/documentAtestado">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>  
              </tbody>
            </table>  
          </div>
        </div>
      </div>

@endsection