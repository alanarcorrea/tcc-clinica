<img src="images/logo-healer.png" class="img-rounded">

<h5>Relatório de Pacientes</h5>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-mail</th>
      <th>Telefone</th>
      <th>Convênio</th>
    </tr>
  </thead>
  <tbody>
    @foreach($pacients as $p)
      <tr>
        <td>{{ $p->name }}</td>
        <td>{{ $p->email }}</td>
        <td>{{ $p->cellPhone }}</td>
        <td>
          @foreach ($agreements as $a)
              @if ($a->id == $p->id_agreements)
                {{ $a->name }}
              @endif
          @endforeach
        </td>
      </tr>  
    @endforeach
  </tbody>
</table>