@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Convênios</li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-building"></i> Convênios
        </div>
        <a href="{{ route('convenios.create') }}" class="btn btn-success">Novo convênio</a>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </tfoot>
              <tbody>

                @foreach ($agreements as $a)
                  <tr>
                    <td>{{ $a->name }}</td>
                    <td style="text-align: center; width: 80px">
                      <a href="/convenios/{{ $a->id }}/edit">
                        <i class="fa fa-edit"></i>
                      </a>
  
                      <form style="display: inline-block;" method="POST" action="{{ route('convenios.destroy', $a->id) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
  
                        <button type="submit" class="custom-btn">
                          <i class="fa fa-trash"></i>
                        </button>
                      </form>
                    </td>
                  </tr>
                @endforeach   
              </tbody>
            </table>
            
          </div>
        </div>
      </div>

@endsection