<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Healer</title>

  <script src="{{ asset('template/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('template/js/sb-admin.min.js') }}"></script>

  {{Html::style('template/vendor/bootstrap/css/bootstrap.min.css')}}
  {{Html::style('template/vendor/font-awesome/css/font-awesome.min.css')}}

  <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('template/css/sb-admin.css') }}" rel="stylesheet">
  <link href="{{ asset('template/css/alter.css') }}" rel="stylesheet">
  
  <link href="template/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
 <script src="{{ asset('template/js/lib/jquery.min.js') }}"></script>

</head>

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        @yield('container')
    </body>

</html>