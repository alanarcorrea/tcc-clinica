<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Healer</title>
  <!-- Bootstrap core CSS-->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/css/sb-admin.css" rel="stylesheet">
  <link href="/css/alter.css" rel="stylesheet">

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/dashboard" style="display: flex; align-items: center;">
      <span style="background: url('/images/healer-small-white.png') center no-repeat; background-size: contain; display: inline-block; width: 45.7px; height: 30px; margin-right: 5px"></span> Healer
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <!-- Início Menu Lateral-->
      @include('sidebar')
      <!-- ^Fim Menu Lateral-->

      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Sair</a>
        </li>
      </ul>
    </div>
  </nav>
  
  <div class="content-wrapper">
    <div class="container-fluid">

     @yield('container')

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright ©Healer 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deseja realmente sair?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Selecione "Sair" abaixo se você deseja realmente sair.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="javascript:;"
              onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">              
            {{ __('Sair') }}</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript-->
    
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/vendor/chart.js/Chart.min.js"></script>
    <script src="/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/js/sb-admin-datatables.min.js"></script>
    <script src="/js/sb-admin-charts.min.js"></script>

    <script src="{{ asset('js/jquery.mask.js') }}" defer></script>

    @yield('extra_scripts')

    <script>
      window.onload = function() {
        $('#cep').mask('00000-000');
        $('#phone').mask('0000-0000');
        $('#cpf').mask('000.000.000-00', {reverse: true});
        $('#cnpj').mask('00,.000.000/0000-00', {reverse: true});
        $('#cellPhone').mask('(00) 00000-0000');
        $('#homePhone').mask('(00) 0000-0000');
        $('#salary, #value').mask('000.000.000.000.000,00', {reverse: true});
        
        const events = document.querySelectorAll('.fc-event');

        events.forEach(event => {
          event.addEventListener('click', ({ currentTarget }) => {
            const title = currentTarget.querySelector('.fc-title').textContent;
            const id = title.split(' ').pop().replace('(', '').replace(')', '');
            window.location.href = `/agenda/${id}`;
          });
        });



        const procedures = document.querySelector('#id_clinical_procedures');
        const procedureValueEl = document.querySelector('#valueClinicalProcedure');
        const discountValueEl = document.querySelector('#discount');
        const totalValueEl = document.querySelector('#total_value');

        procedures.addEventListener('change', ev => {
            const target = ev.target;
            const option = ev.target.options[ev.target.selectedIndex];
            const value = parseFloat(option.getAttribute('data-value'));

            procedureValueEl.value = value.toFixed(2);
            totalValueEl.value = (
              (value + 120) - ((value + 120) * discountValueEl.value / 100)
            ).toFixed(2);
        });
      }
    </script>

  </div>
</body>

</html>
