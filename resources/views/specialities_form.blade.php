@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item">
          <a href="index.html">Especialidades</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Nova especialidade
            @else
                Editar especialidade
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('especialidades.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('especialidades.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Nome:</label>
            <div class="col-md-5">
                <input type="name" class="form-control" id="name" name="name" placeholder="Nome da especialidade" value="{{ $reg->name or old('name') }}">
            </div>
        </div>

        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Salvar</button>
            </div>
        </div>
    </form>

@endsection