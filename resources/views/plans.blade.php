@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Planos</li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-building"></i> Planos
        </div>
        <a href="{{ route('planos.create') }}" class="btn btn-success">Novo Plano</a>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Convênio</th>
                  <th>Desconto Consulta</th>
                  <th>Desconto Procedimentos Clínicos</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nome</th>
                  <th>Convênio</th>
                  <th>Desconto Consulta</th>
                  <th>Desconto Procedimentos Clínicos</th>
                  <th>Ações</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($plans as $plan)
                  <tr>
                    <td>{{ $plan->name }}</td>
                    <td>
                      @foreach ($agreements as $a)
                          @if ($a->id == $plan->id_agreements)
                            {{ $a->name }}
                          @endif
                      @endforeach
                    </td>
                    <td>{{ $plan->discountConsultation }}%</td>
                    <td>{{ $plan->discountProcedure }}%</td>
                    <td style="text-align: center;">
                      <a href="/planos/{{ $plan->id }}/edit">
                        <i class="fa fa-edit"></i>
                      </a>

                      <form style="display: inline-block;" method="POST" action="{{ route('planos.destroy', $plan->id) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}

                        <button type="submit" class="custom-btn">
                          <i class="fa fa-trash"></i>
                        </button>
                      </form>
                    </td>
                  </tr>  
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

@endsection