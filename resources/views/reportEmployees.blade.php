<img src="images/logo-healer.png" class="img-rounded">

<h5>Relatório de Funcionários</h5>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-mail</th>
      <th>Telefone</th>
      <th>Salário</th>
    </tr>
  </thead>
  <tbody>
    @foreach($employees as $e)
      <tr>
        <td>{{ $e->name }}</td>
        <td>{{ $e->email }}</td>
        <td>{{ $e->cellPhone }}</td>
        <td>{{ $e->salary }}</td>
      </tr>  
    @endforeach
  </tbody>
</table>