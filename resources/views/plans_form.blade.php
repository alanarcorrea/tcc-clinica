@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item">
          <a href="index.html">Planos</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Novo plano
            @else
                Editar plano
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('planos.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('planos.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Nome:</label>
            <div class="col-md-5">
                <input type="name" class="form-control" id="name" name="name" placeholder="Nome do plano" value="{{ $reg->name or old('name') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="discountConsultation">Desconto Consultas (%):</label>
            <div class="col-md-5"> 
                <input type="number" class="form-control" id="discountConsultation" placeholder="Desconto" name="discountConsultation" value="{{ $reg->discountConsultation or old('discountConsultation') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="discountProcedure">Desconto Procedimentos Clínicos (%):</label>
            <div class="col-md-5"> 
                <input type="number" class="form-control" id="discountProcedure" placeholder="Desconto" name="discountProcedure" value="{{ $reg->discountProcedure or old('discountProcedure') }}">
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-4" for="id_agreements">Convênio</label>
          
          <div class="col-md-5">
            <select class="form-control" id="id_agreements" name="id_agreements">
                <option value="0">Selecione</option>
                @foreach ($agreements as $a)
                <option value="{{ $a->id }}" @if ((isset ($reg) and $a->id == $reg->id_agreements) or old('id_agreements') == $a->id) selected @endif>{{ $a->name }}</option>
                @endforeach
            </select>
          </div>          
        </div>

        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Salvar</button>
            </div>
        </div>
    </form>

@endsection