@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('clinica.index') }}">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Dados Gerais</li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-building"></i> Clínica
        </div>
        
        @if (isset($clinics->id))
          <a href="/clinica/{{$clinics->id}}/edit" class="btn btn-success">Editar</a>
        @else
          <a href="/clinica/create" class="btn btn-success">Nova Clínica</a>
        @endif

      <div class="row clinic-container">
        <div class="col-12">
          @if (isset($clinics->id))
            <h2>{{ $clinics->fantasyName }}</h2>
            <hr>
            <h5>Razão Social:</h5>
            <p>{{ $clinics->socialName }}</p>
            <h5>CNPJ:</h5>
            <p>{{ $clinics->cnpj }}</p>
            <h5>Telefone:</h5>
            <p>{{ $users->homePhone }} / {{ $users->cellPhone }}</p>
            <h5>E-mail:</h5>
            <p>{{ $users->email }}</p>
            <h5>Endereço:</h5>
            <p>{{ $users->address }} - {{ $users->city }} - {{ $users->cep }}</p>
            <h5>Dados Financeiros:</h5>
            <p>{{ $clinics->bank }} / {{ $clinics->agency }}-{{ $clinics->bankAccount }}</p>
          @else
            <p>Não há clínica cadastrada!</p>
          @endif
        </div>
      </div>

@endsection