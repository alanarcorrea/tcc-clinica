@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Procedimentos Clínicos</li>
      </ol>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-building"></i> Procedimentos Clínicos
        </div>
        <a href="{{ route('procedimentos.create') }}" class="btn btn-success">Novo Procedimento Clínico</a>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Título</th>
                  <th>Valor (R$)</th>
                  <th>Observações</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </thead>

                @foreach ($procedures as $p)
                  <tr>
                    <td>{{ $p->title }}</td>
                    <td>R$ {{ number_format($p->value, 2, ',', '.') }}</td>

                    <td>{{ $p->note }}</td>
                    <td style="text-align: center; width: 80px">
                      <a href="/procedimentos/{{ $p->id }}/edit">
                        <i class="fa fa-edit"></i>
                      </a>
  
                      <form style="display: inline-block;" method="POST" action="{{ route('procedimentos.destroy', $p->id) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
  
                        <button type="submit" class="custom-btn">
                          <i class="fa fa-trash"></i>
                        </button>
                      </form>
                    </td>
                  </tr>
                @endforeach   
              </tbody>
            </table>
            
          </div>
        </div>
      </div>

@endsection