@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('clinica.index') }}">Clínica</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('clinica.index') }}">Dados Gerais</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Nova Clínica
            @else
                Editar Clínica
            @endif
        </li>
      </ol>


        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('clinica.store') }}" method="POST">
            
        @else
            <form class="form" action="{{ route('clinica.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
            <input type="hidden" name="id_users" value="{{ $id_users }}"/>
        @endif
      
        {{ csrf_field() }}
        

        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Nome Social:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nome Social" value="{{ $reg->name or old('name') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="fantasyName">Nome Fantasia:</label>
            <div class="col-md-5"> 
                <input type="text" class="form-control" id="fantasyName" placeholder="Nome Fantasia" name="fantasyName" value="{{ $reg->fantasyName or old('fantasyName') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="cnpj">CNPJ:</label>
            <div class="col-md-5"> 
                <input type="tel" class="form-control" id="cnpj" placeholder="CNPJ" name="cnpj" value="{{ $reg->cnpj or old('cnpj') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="birthDate">Data Inauguração:</label>
            <div class="col-md-5"> 
                <input type="date" class="form-control" id="birthDate" placeholder="Data Inauguração" name="birthDate" value="{{ $reg->birthDate or old('birthDate') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="cellPhone">Telefone Celular:</label>
            <div class="col-md-5"> 
                <input type="tel" class="form-control" id="cellPhone" placeholder="Telefone Celular" name="cellPhone" value="{{ $reg->cellPhone or old('cellPhone') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="homePhone">Telefone Residencial:</label>
            <div class="col-md-5"> 
                <input type="tel" class="form-control" id="homePhone" placeholder="Telefone Residencial" name="homePhone" value="{{ $reg->homePhone or old('homePhone') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">E-mail:</label>
            <div class="col-md-5"> 
                <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" value="{{ $reg->email or old('email') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="address">Endereço:</label>
            <div class="col-md-5"> 
                <input type="text" class="form-control" id="address" placeholder="Endereço" name="address" value="{{ $reg->address or old('address') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="city">Cidade:</label>
            <div class="col-md-5"> 
                <input type="text" class="form-control" id="city" placeholder="Cidade" name="city" value="{{ $reg->city or old('city') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="cep">CEP:</label>
            <div class="col-md-5"> 
                <input type="tel" max-length="11" class="form-control" id="cep" placeholder="CEP" name="cep" value="{{ $reg->cep or old('cep') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="bank">Banco:</label>
            <div class="col-md-5"> 
                <input type="text" class="form-control" id="bank" placeholder="Banco" name="bank" value="{{ $reg->bank or old('bank') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="agency">Agência:</label>
            <div class="col-md-5"> 
                <input type="number" class="form-control" id="agency" placeholder="Agência" name="agency" value="{{ $reg->agency or old('agency') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="bankAccount">Conta:</label>
            <div class="col-md-5"> 
                <input type="number" class="form-control" id="bankAccount" placeholder="Conta" name="bankAccount" value="{{ $reg->bankAccount or old('bankAccount') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="password">Senha:</label>
            <div class="col-md-5"> 
                <input type="password" class="form-control" id="password" placeholder="Senha" name="password" value="{{ $reg->password or old('password') }}" required>
            </div>
        </div>

        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Salvar</button>
            </div>
        </div>
                
    </form>

    @if ($action == 2)
        <form style="display: inline-block;" method="POST" action="{{ route('clinica.destroy', $reg->id ) }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <input type="hidden" name="id_users" value="{{ $id_users }}"/>

            <button type="submit" class="btn btn-danger">Excluir</button>
        </form>
    @endif
@endsection