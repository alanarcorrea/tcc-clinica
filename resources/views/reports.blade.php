@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/relatorios">Relatório</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-clipboard"></i> Relatório          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                 <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Relatório de Pacientes | Total</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportPacients">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Relatório de Funcionários | Total</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportEmployees">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Relatório de Convênios | Total</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportAgreements">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Relatório de Planos | Total</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportPlans">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Relatório de Contas a Receber | Mensal | Categoria</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportBillsReceiptsCategories">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>     
                <tr>
                    <tr>
                        <td>Relatório de Contas a Pagar | Mensal | Categoria</td>
                        <td style="text-align: center; width: 80px">
                          <a href="/relatorios/reportBillsPaymentsCategories">
                            <i class="fa fa-print"></i>
                          </a>
                        </td>
                      </tr>     
                      <tr>
                  <td>Relatório de Atendimentos | Mensal </td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportCallsMonth">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>  
                <tr>
                  <td>Relatório de Atendimentos | Mensal | Convênios</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportCallsMonthAgreement">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>   
                <tr>
                  <td>Relatório de Atendimentos | Mensal | Funcionários</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportCallsMonthEmployee">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>   
                <tr>
                  <td>Relatório de Atendimentos | Mensal | Pacientes</td>
                  <td style="text-align: center; width: 80px">
                    <a href="/relatorios/reportCallsMonthPacient">
                      <i class="fa fa-print"></i>
                    </a>
                  </td>
                </tr>          
              </tbody>
            </table>  
          </div>
        </div>
      </div>

@endsection