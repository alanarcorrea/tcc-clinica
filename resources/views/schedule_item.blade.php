@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Agenda</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Agendar Consulta Médica
            @else
                Editar Consulta Médica
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('agenda.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('agenda.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}


        <div class="form-group">
                <label class="control-label col-sm-4" for="specialty">Especialidade:</label>
                <div class="col-md-5">
                    <input type="text" class="form-control" id="specialty" name="specialty" value="{{ $specialty }}" disabled>
                </div>   
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_employees">Profissional:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="employee" name="employee" value="{{ $employee }}" disabled>
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="pacient">Paciente:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="pacient" name="pacient" value="{{ $schedule->pacient }}" disabled>
            </div>   
        </div> 

        
        <div class="form-group">
            <label class="control-label col-sm-4" for="date">Data da Consulta:</label>
            <div class="col-md-5">
                <input type="date" class="form-control" id="date" name="date" placeholder="Data" value="{{ $schedule->date }}" disabled>
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="hour">Hora da Consulta:</label>
            <div class="col-md-5">
                <input type="time" class="form-control" id="hour" name="hour" placeholder="Hora" value="{{ $schedule->hour }}" disabled>
            </div>   
        </div> 

        
        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
            <a href="/atendimentos/{{ $schedule_id }}" class="btn btn-default">Iniciar Atendimento</a>
            </div>
        </div>
    </form>

   


    <form style="display: inline-block;" method="POST" action="{{ route('agenda.destroy', $schedule->id ) }}">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn btn-danger">Excluir</button>
    </form>
@endsection