@extends('base')

@section('container')

<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('pacientes.index') }}">Paciente</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Novo Paciente
            @else
                Editar Paciente
            @endif
        </li>
    </ol>

    @if ($action == 1)
        <form class="form-horizontal" action="{{ route('pacientes.store') }}" method="POST">
        
    @else
        <form class="form" action="{{ route('pacientes.update', $reg->id) }}" method="post">
        {!! method_field('put') !!}
        <input type="hidden" name="id_users" value="{{ $reg->id_users }}"/>
    @endif
      
        {{ csrf_field() }}

        <div class="card mb-3 custom-items">
            <div class="list-group list-group-flush small">
                <div class="list-group-item list-item-center">
                    {{-- <img class="d-flex rounded-circle" src="https://images.unsplash.com/photo-1513732822839-24f03a92f633?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c019fe313afd6ef860d09b17e8066559&auto=format&fit=crop&w=1534&q=80" alt=""> --}}

                <img class="rounded-circle list-group-item list-item-center"  style="width: 64px; padding: 0; height: 64px; margin-top: 20px;" src="/images/logo-square.png" alt="">
                </div>
                <div class="grid-50 --vertical-space">
                    <div class="line ">
                        <div class="media-body">
                            <h5>Nome Completo:</h5>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nome Completo" value="{{ $reg->name or old('name') }}" required>
                        </div>
                    </div>
                    <div class="line ">
                        <div >
                            <h5>E-mail:</h5>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ $reg->email or old('email') }}" required>
                            </div>    
                        <div >    
                            <h5>Data de Nascimento:</h5>
                            <input type="date" class="form-control" id="birthDate" name="birthDate" placeholder="Data de Nascimento" value="{{ $reg->birthDate or old('birthDate') }}" required>
                        </div>
                        <div >
                            <h5>Senha:</h5>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Senha" value="{{ $reg->password or old('password') }}" required>
                        </div> 
                    </div>
                    <div class="line">     
                        <div >
                            <h5>Tel. Celular:</h5>
                            <input type="tel" class="form-control" id="cellPhone" name="cellPhone" placeholder="Telefone" value="{{ $reg->cellPhone or old('cellPhone') }}" required>
                        </div>
                        <div >
                            <h5>Tel. Residencial:</h5>
                            <input type="tel" class="form-control" id="homePhone" name="homePhone" placeholder="Telefone" value="{{ $reg->homePhone or old('homePhone') }}" required>
                            </div>  
                        <div >   
                            <h5>CPF:</h5>
                            <input type="tel" class="form-control" id="cpf" name="cpf" placeholder="CPF" value="{{ $reg->cpf or old('cpf') }}" required>
                        </div>
                    </div>  
                    <div class="line">     
                            <div >
                                <h5>Endereço:</h5>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Endereço" value="{{ $reg->address or old('address') }}" required>
                            </div>
                            <div >
                                <h5>Cidade:</h5>
                                <input type="text" class="form-control" id="city" name="city" placeholder="Cidade" value="{{ $reg->city or old('city') }}" required>
                                </div>  
                            <div >   
                                <h5>CEP:</h5>
                                <input type="tel" class="form-control" id="cep" name="cep" placeholder="CEP" value="{{ $reg->cep or old('cep') }}" required>
                            </div>
                        </div>   
                    <div class="line ">
                            <div >
                                <h5>Convênio:</h5> 
                                    <select class="form-control" id="id_agreements" name="id_agreements">
                                        <option value="0">Selecione</option>
                                        @foreach ($agreements as $a)
                                            <option value="{{ $a->id }}" @if ((isset ($reg) and $a->id == $reg->id_agreements) or old('id_agreements') == $a->id) selected @endif>{{ $a->name }}</option>
                                        @endforeach
                                    </select>     
                            </div>    
                            <div >    
                                <h5>Plano:</h5>
                                <select class="form-control" id="id_plans" name="id_plans">
                                    <option value="0">Selecione</option>
                                        @foreach ($plans as $p)
                                        <option value="{{ $p->id }}" @if ((isset ($reg) and $p->id == $reg->id_plans) or old('id_plans') == $p->id) selected @endif>{{ $p->name }}</option>
                                        @endforeach
                                </select>   
                            </div>
                            <div >    
                                <h5>RG:</h5>
                                <input type="number" class="form-control" id="rg" name="rg" placeholder="RG" value="{{ $reg->rg or old('rg') }}" required>    
                            </div> 
                    </div>  
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-default">Salvar</button>
    </form>

    @if ($action == 2)
        <form style="display: inline-block;" method="POST" action="{{ route('pacientes.destroy', $reg->id) }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <input type="hidden" name="id_users" value="{{ $reg->id_users }}"/>

            <button type="submit" class="btn btn-danger">Excluir</button>
        </form>
    @endif

@endsection
@section('extra_scripts')
    <script>
        
    </script>
@endsection