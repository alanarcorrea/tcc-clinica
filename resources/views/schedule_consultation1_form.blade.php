@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Agenda</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Agendar Consulta Médica
            @else
                Editar Consulta Médica
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('agenda.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('agenda.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        <h4>Selecione a especialidade desejada para a consulta médica: </h4>

        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                   <tr>
                    <th>Nome</th>
                    <th style="width: 80px">Avançar</th>
                  </tr>
                </thead>
                <tfoot>
                   <tr>
                    <th>Nome</th>
                    <th style="width: 80px">Avançar</th>
                  </tr>
                </tfoot>
                <tbody>
                    @foreach ($specialities as $s)
                      <tr>
                        <td>{{ $s->name }}</td>
                        <td style="text-align: center; width: 80px">
                            <a href="consulta/{{$s->id}}/profissional/">
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </td>
                      </tr>
                    @endforeach       
                </tbody>
              </table>  
            </div>

    </form>

@endsection