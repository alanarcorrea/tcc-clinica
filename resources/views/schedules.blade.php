@extends('base')

@section('container')
<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/agenda">Agenda</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-circle"></i> Agenda
        </div>
        <a href="/agenda/consulta" class="btn btn-success">Agendar Consulta Médica</a>

        {!! $calendar->calendar() !!}
        {!! $calendar->script() !!}

      </div>
@endsection      