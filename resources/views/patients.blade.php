@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Pacientes</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-circle"></i> Pacientes
        </div>
        <a href="{{ route('pacientes.create') }}" class="btn btn-success">Novo Paciente</a>

        @foreach ( $pacients as $p )
          <a class="list-group-item list-group-item-action" href="/pacientes/{{$p->id}}/edit">
            <div class="media">
              <img class="d-flex mr-3 rounded-circle" src="/images/logo-square.png" alt="">
              <div class="media-body">
                <strong>{{ $p->name }}</strong>
                <div class="text-muted smaller">{{ $p->city }} - {{ $p->cellPhone }}</div>
              </div>
            </div>
          </a>
        @endforeach

          </div>
        </div>
      </div>

@endsection      