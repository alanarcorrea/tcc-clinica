@extends('base')

@section('container')

<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html">Financeiro</a>
        </li>
        <li class="breadcrumb-item active">Contas a Pagar</li>
    </ol>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-money"></i> Contas a Pagar
        </div>
        <a href="{{ route('despesas.create') }}" class="btn btn-success">Nova Despesa</a>
        <div class="card-body">
        <h5>Total (R$): {{ number_format($totalDespesas->total, 2, ',', '.') }}</h5>
        <hr>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Data do Pagamento</th>
                            <th>Valor (R$)</th>
                            <th style="width: 80px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($despesas as $d)
                            <tr>
                                <td>{{ $d->title }}</td>
                                <td>{{ date('d/m/Y', strtotime($d->payDay)) }}</td>
                                <td>R$ {{ number_format($d->value, 2, ',', '.') }}</td>
                                <td style="text-align: center; width: 80px">
                                <a href="/despesas/{{ $d->id }}/edit">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <form style="display: inline-block;" method="POST" action="{{ route('despesas.destroy', $d->id) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}

                                    <button type="submit" class="custom-btn">
                                    <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                                </td>
                            </tr>
                        @endforeach       
                    </tbody>
                </table>  
            </div>
        </div>
    </div>

@endsection