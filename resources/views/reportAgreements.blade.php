<img src="images/logo-healer.png" class="img-rounded">

<h5>Relatório de Convênios</h5>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Nome</th>
    </tr>
  </thead>
  <tbody>
    @foreach($agreements as $a)
      <tr>
        <td>{{ $a->name }}</td>
      </tr>  
    @endforeach
  </tbody>
</table>