<img src="images/logo-healer.png" class="img-rounded">

<h5 style="font-size: 24px; text-align: center; display: block;">Relatório de Contas a Pagar | Mensal | Categoria</h5>

<table width="100%" cellspacing="0" style="font-size: 18px">
  <thead>
    <tr>
      <th>Categoria</th>
      <th>Mês</th>
      <th>Valor</th>
    </tr>
  </thead>
  <tbody>
    @foreach($bills as $b)
      <tr>
        <td>{{ $b->categoria }}</td>
        <td>{{ $b->mes }}</td>
        <td>{{ number_format($b->valortotal, 2, ',', '.') }}</td>
      </tr>  
    @endforeach
  </tbody>
</table>

<hr>

<table width="100%" cellspacing="0" style="font-size: 18px">
  <thead>
    <tr>
      <th>Categoria</th>
      <th>Valor Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($total as $t)
      <tr>
        <td>{{ $t->categoria }}</td>
        <td>{{ number_format($t->valortotal, 2, ',', '.') }}</td>
      </tr>  
    @endforeach
  </tbody>
</table>

<span style="display: block; margin: 100px auto 50px; text-align: center;">Relatório emitido em: {{ date('d/m/Y') }}</span>