@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item">
          <a href="index.html">Procedimentos Clínicos</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Novo Procedimento Clínico
            @else
                Editar Procedimento Clínico
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('procedimentos.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('procedimentos.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label col-sm-4" for="title">Título:</label>
            <div class="col-md-5">
                <input type="title" class="form-control" id="title" name="title" placeholder="Título do procedimento" value="{{ $reg->title or old('title') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="value">Valor (R$):</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="value" name="value" placeholder="Valor do procedimento" value="{{ $reg->value or old('value') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_specialities">Especialidade:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_specialities" name="id_specialities">
                    <option value="0">Selecione</option>
                    @foreach ($specialities as $s)
                        <option value="{{ $s->id }}" @if ((isset ($reg) and $s->id == $reg->id_specialities) or old('id_specialities') == $s->id) selected @endif>{{ $s->name }}</option>
                    @endforeach
                </select>   
            </div>      
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="note">Observações:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="note" name="note" placeholder="Observações" value="{{ $reg->note or old('note') }}">
            </div>
        </div>

        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Salvar</button>
            </div>
        </div>
    </form>

@endsection