<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
    @if ($user->type != 3)
    <!-- Menu Dashboard-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
        <i class="fa fa-fw fa-dashboard"></i>
        <span class="nav-link-text">Dashboard</span>
        </a>
    </li>
    @endif

    @if ($user->type == 1)
    <!-- Menu Clínica-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clínica">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#dropClinica" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-building"></i>
        <span class="nav-link-text">Clínica</span>
        </a>
        <ul class="sidenav-second-level collapse" id="dropClinica">
        <li>
            <a href="{{ route('clinica.index') }}">Dados Gerais</a>
        </li>
        <li>
            <a href="{{ route('especialidades.index') }}">Especialidades</a>
        </li>
        <li>
            <a href="{{ route('convenios.index') }}">Convênios</a>
        </li>
        <li>
            <a href="{{ route('planos.index') }}">Planos</a>
        </li>
        <li>
            <a href="{{ route('funcionarios.index') }}">Funcionários</a>
        </li>
        <li>
            <a href="{{ route('procedimentos.index') }}">Procedimentos Clínicos</a>
        </li>
        <li>
            <a href="/documentos">Modelo de Documentos</a>
        </li>
        </ul>
    </li>
    @endif

    @if ($user->type != 3)
    <!-- Menu Paciente-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Paciente">
        <a class="nav-link" href="{{ route('pacientes.index') }}">
        <i class="fa fa-fw fa-users"></i>
        <span class="nav-link-text">Paciente</span>
        </a>
    </li>
    @endif

    @if ($user->type == 1)
    <!-- Menu Financeiro-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Financeiro">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#dropFinanceiro" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-money"></i>
        <span class="nav-link-text">Financeiro</span>
        </a>
        <ul class="sidenav-second-level collapse" id="dropFinanceiro">
        <li>
            <a href="{{ route('categorias.index') }}">Categorias</a>
        </li>
        <li>
            <a href="{{ route('despesas.index') }}">Contas a Pagar</a>
        </li>
        <li>
            <a href="{{ route('receitas.index') }}">Contas a Receber</a>
        </li>
        </ul>
    </li>
    @endif

    @if ($user->type != 3)
    <!-- Menu Agenda-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agenda">
        <a class="nav-link" href="{{ route('agenda.index') }}">
        <i class="fa fa-fw fa-calendar"></i>
        <span class="nav-link-text">Agenda</span>
        </a>
    </li>
    @endif

    @if ($user->type == 1)
    <!-- Menu Relatório-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Relatorio">
        <a class="nav-link" href="/relatorios">
        <i class="fa fa-fw fa-clipboard"></i>
        <span class="nav-link-text">Relatório</span>
        </a>
    </li>
    @endif

    @if ($user->type == 3)
        <!-- Menu Histórico-->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Histórico">
            <a class="nav-link" href="/historico">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Histórico</span>
            </a>
        </li>
    @endif    

    @if ($user->type != 3)
    <!-- Menu Ajuda-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Ajuda">
        <a class="nav-link" href="/ajuda">
        <i class="fa fa-fw fa-book"></i>
        <span class="nav-link-text">Ajuda</span>
        </a>
    </li>
    @endif

</ul>