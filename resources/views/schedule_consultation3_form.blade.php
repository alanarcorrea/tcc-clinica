@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Agenda</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Agendar Consulta Médica
            @else
                Editar Consulta Médica
            @endif
        </li>
      </ol>

        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('agenda.store') }}" method="POST">
        @else
            <form class="form" action="{{ route('agenda.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
        @endif
      
        {{ csrf_field() }}

        
        <h4>Selecione o paciente e a data da consulta com o profissional : </h4>
        
        <input type="text" name="user_id_employee" id="user_id_employee" value="{{ $idEmployee }}" hidden/>
        
        <div class="form-group">
                <label class="control-label col-sm-4" for="specialty">Especialidade:</label>
                <div class="col-md-5">
                    <input type="text" class="form-control" id="specialty" name="specialty" value="{{ $specialty->name }}">
                </div>   
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_employees">Profissional:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="employee" name="employee" value="{{ $employee->name }}">
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="id_pacients">Paciente:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_pacients" name="id_pacients">
                    <option value="0">Selecione</option>
                    @foreach ($pacients as $p)
                    <option value="{{ $p->id }}" @if ((isset ($reg) and $p->id == $reg->id_pacients) or old('id_pacients') == $p->id) selected @endif>{{ $p->name }}</option>
                    @endforeach
                </select>
            </div>          
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="date">Data da Consulta:</label>
            <div class="col-md-5">
                <input type="date" class="form-control" id="date" name="date" placeholder="Data" value="{{ $reg->date or old('date') }}">
            </div>   
        </div>  

        <div class="form-group">
            <label class="control-label col-sm-4" for="hour">Hora da Consulta:</label>
            <div class="col-md-5">
                <input type="time" class="form-control" id="hour" name="hour" placeholder="Hora" value="{{ $reg->hour or old('hour') }}">
            </div>   
        </div> 

        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Agendar</button>
            </div>
        </div>

    </form>
@endsection