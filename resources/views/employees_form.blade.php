@extends('base')

@section('container')

<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('funcionarios.index') }}">Funcionário</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Novo Funcionário
            @else
                Editar Funcionário
            @endif
        </li>
    </ol>

    @if ($action == 1)
        <form class="form-horizontal" action="{{ route('funcionarios.store') }}" method="POST">
        
    @else
        <form class="form" action="{{ route('funcionarios.update', $reg->id) }}" method="post">
        {!! method_field('put') !!}
        <input type="hidden" name="id_users" value="{{ $reg->id_users }}"/>
    @endif
      
        {{ csrf_field() }}

        <div class="card mb-3 custom-items">
            <div class="list-group list-group-flush small">
                <div class="list-group-item list-item-center">
                    <img class="rounded-circle list-group-item list-item-center"  style="width: 64px; padding: 0; height: 64px; margin-top: 20px;" src="/images/logo-square.png" alt="">
                </div>
                <div class="grid-50 --vertical-space">
                    <div class="line ">

                        <div class="media-body">
                            <h5>Nome Completo:</h5>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nome Completo" value="{{ $reg->name or old('name') }}" required>
                        </div>

                    </div>
                    <div class="line ">
                            <div >
                                    <h5>Função:</h5> 
                                    <select class="form-control" id="id_functions" name="id_functions">
                                        <option value="0">Selecione</option>
                                        @foreach ($functions as $f)
                                            <option value="{{ $f->id }}" @if ((isset ($reg) and $f->id == $reg->id_functions) or old('id_functions') == $f->id) selected @endif>{{ $f->name }}</option>
                                        @endforeach
                                    </select>     
                            </div>  
                            <div class="media-body">
                                    <h5>CRM:</h5>
                                    <input type="text" class="form-control" id="crm" name="crm" placeholder="CRM" value="{{ $reg->crm or old('crm') }}" required>
                                </div>
                        </div>
                    <div class="line ">
                        <div >
                            <h5>E-mail:</h5>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ $reg->email or old('email') }}" required>
                            </div>    
                        <div >    
                            <h5>Data de Nascimento:</h5>
                            <input type="date" class="form-control" id="birthDate" name="birthDate" placeholder="Data de Nascimento" value="{{ $reg->birthDate or old('birthDate') }}" required>
                        </div>
                        <div >
                            <h5>Senha:</h5>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Senha" value="{{ $reg->password or old('password') }}" required>
                        </div> 
                    </div>
                    <div class="line">     
                        <div >
                            <h5>Tel. Celular:</h5>
                            <input type="tel" class="form-control" id="cellPhone" name="cellPhone" placeholder="Telefone" value="{{ $reg->cellPhone or old('cellPhone') }}" required>
                        </div>
                        <div >
                            <h5>Tel. Residencial:</h5>
                            <input type="tel" class="form-control" id="homePhone" name="homePhone" placeholder="Telefone" value="{{ $reg->homePhone or old('homePhone') }}" required>
                            </div>  
                        <div >   
                            <h5>CPF:</h5>
                            <input type="tel" class="form-control" id="cpf" name="cpf" placeholder="CPF" value="{{ $reg->cpf or old('cpf') }}" required>
                        </div>
                    </div>  
                    <div class="line">     
                            <div >
                                <h5>Endereço:</h5>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Endereço" value="{{ $reg->address or old('address') }}" required>
                            </div>
                            <div >
                                <h5>Cidade:</h5>
                                <input type="text" class="form-control" id="city" name="city" placeholder="Cidade" value="{{ $reg->city or old('city') }}" required>
                                </div>  
                            <div >   
                                <h5>CEP:</h5>
                                <input type="tel" class="form-control" id="cep" name="cep" placeholder="CEP" value="{{ $reg->cep or old('cep') }}" required>
                            </div>
                        </div>   
                    <div class="line ">
                            <div >   
                                <h5>CTPS:</h5>
                                <input type="number" class="form-control" id="ctps" name="ctps" placeholder="CTPS" value="{{ $reg->ctps or old('ctps') }}" required>
                            </div>    
                            <div >    
                                <h5>RG:</h5>
                                <input type="number" class="form-control" id="rg" name="rg" placeholder="RG" value="{{ $reg->rg or old('rg') }}" required>    
                            </div> 
                            <div >   
                                <h5>Salário:</h5>
                                <input type="tel" class="form-control" id="salary" name="salary" placeholder="Salário" value="{{ $reg->salary or old('salary') }}" required>
                            </div>
                    </div> 
                    <div class="line ">
                        <div >   
                            <h5>Banco:</h5>
                            <input type="text" class="form-control" id="bank" name="bank" placeholder="Banco" value="{{ $reg->bank or old('bank') }}" required>
                        </div>    
                        <div >    
                            <h5>Agência:</h5>
                            <input type="number" class="form-control" id="agency" name="agency" placeholder="Agência" value="{{ $reg->agency or old('agency') }}" required>    
                        </div> 
                        <div >   
                            <h5>Conta Bancária:</h5>
                            <input type="number" class="form-control" id="bankAccount" name="bankAccount" placeholder="Conta Bancária" value="{{ $reg->bankAccount or old('bankAccount') }}" required>
                        </div>
                    </div>
                    <div class="line">
                        <div >
                            <h5>1º Especialidade:</h5> 
                            <select class="form-control" id="specialty1" name="specialty1">
                                <option value="0">Selecione</option>
                                @foreach ($specialities as $s)
                                    <option value="{{ $s->id }}" @if ((isset ($reg) and $s->id == $reg->specialty1) or old('specialty1') == $s->id) selected @endif>{{ $s->name }}</option>
                                @endforeach
                            </select>     
                        </div>
                        <div >
                            <h5>2º Especialidade:</h5> 
                            <select class="form-control" id="specialty2" name="specialty2">
                                <option value="0">Selecione</option>
                                @foreach ($specialities as $s)
                                    <option value="{{ $s->id }}" @if ((isset ($reg) and $s->id == $reg->specialty2) or old('specialty2') == $s->id) selected @endif>{{ $s->name }}</option>
                                @endforeach
                            </select>     
                        </div>
                        <div >
                            <h5>3º Especialidade:</h5> 
                            <select class="form-control" id="specialty3" name="specialty3">
                                <option value="0">Selecione</option>
                                @foreach ($specialities as $s)
                                    <option value="{{ $s->id }}" @if ((isset ($reg) and $s->id == $reg->specialty3) or old('specialty3') == $s->id) selected @endif>{{ $s->name }}</option>
                                @endforeach
                            </select>     
                        </div>    
                    </div>  
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-default">Salvar</button>
    </form>

    @if ($action == 2)
        <form style="display: inline-block;" method="POST" action="{{ route('funcionarios.destroy', $reg->idEmp) }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <input type="hidden" name="id_users" value="{{ $reg->id_users }}"/>

            <button type="submit" class="btn btn-danger">Excluir</button>
        </form>
    @endif

@endsection
@section('extra_scripts')
    <script>
        
    </script>
@endsection