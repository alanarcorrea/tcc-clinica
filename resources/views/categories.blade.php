@extends('base')

@section('container')

<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Categorias</li>
    </ol>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-money"></i> Categorias
        </div>
        <a href="{{ route('categorias.create') }}" class="btn btn-success">Nova categoria</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th style="width: 80px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $c)
                            <tr>
                                <td>{{ $c->name }}</td>
                                <td style="text-align: center; width: 80px">
                                <a href="/categorias/{{ $c->id }}/edit">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <form style="display: inline-block;" method="POST" action="{{ route('categorias.destroy', $c->id) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}

                                    <button type="submit" class="custom-btn">
                                    <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                                </td>
                            </tr>
                        @endforeach       
                    </tbody>
                </table>  
            </div>
        </div>
    </div>

@endsection