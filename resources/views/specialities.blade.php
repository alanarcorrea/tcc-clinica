@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Clínica</a>
        </li>
        <li class="breadcrumb-item active">Especialidades</li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-building"></i> Especialidades          
        </div>
        <a href="{{ route('especialidades.create') }}" class="btn btn-success">Nova especialidade</a>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                 <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </thead>
              <tfoot>
                 <tr>
                  <th>Nome</th>
                  <th style="width: 80px">Ações</th>
                </tr>
              </tfoot>
              <tbody>
                  @foreach ($specialities as $s)
                    <tr>
                      <td>{{ $s->name }}</td>
                      <td style="text-align: center; width: 80px">
                        <a href="/especialidades/{{ $s->id }}/edit">
                          <i class="fa fa-edit"></i>
                        </a>
    
                        <form style="display: inline-block;" method="POST" action="{{ route('especialidades.destroy', $s->id) }}">
                          {{ method_field('DELETE') }}
                          {{ csrf_field() }}
    
                          <button type="submit" class="custom-btn">
                            <i class="fa fa-trash"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach       
              </tbody>
            </table>  
          </div>
        </div>
      </div>

@endsection