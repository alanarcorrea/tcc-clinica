@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Histórico</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-clipboard"></i> Histórico          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                 <tr>
                  <th>Data</th>
                  <th>Profissional</th>
                  <th>Prescrição Medicamento</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($history as $h)
                      <tr>
                      <td>{{ date('d/m/Y', strtotime($h->data)) }}</td>
                          <td>{{ $h->profissional }}</td>
                          <td>{{ $h->prescricao }}</td>
                      </tr>
                  @endforeach       
              </tbody>
            </table>  
          </div>
        </div>
      </div>

@endsection