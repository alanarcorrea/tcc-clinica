<img src="images/logo-healer.png" class="img-rounded">

<h5>Relatório de Planos</h5>

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Nome</th>
      <th>Desconto Consulta</th>
      <th>Desconto Procedimento Clínico</th>
      <th>Convênio</th>
    </tr>
  </thead>
  <tbody>
    @foreach($plans as $p)
      <tr>
        <td>{{ $p->name }}</td>
        <td>{{ $p->discountConsultation }}%</td>
        <td>{{ $p->discountProcedure }}%</td>
        <td>
          @foreach ($agreements as $a)
              @if ($a->id == $p->id_agreements)
                {{ $a->name }}
              @endif
          @endforeach
        </td>
      </tr>  
    @endforeach
  </tbody>
</table>