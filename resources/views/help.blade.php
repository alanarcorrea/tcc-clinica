@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/ajuda">Ajuda</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-book"></i> Ajuda
        </div>

        <div class="card-columns">

          <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1524245784377-1e77f3c853e5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=35a60739197ee7cdeef03f136388cb0c&auto=format&fit=crop&w=752&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">CID (Classificação Internacional de Doenças) </a></h6>
                <h6>Códigos de doenças comuns:</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <ol>
                      <li>CID A00 = Cólera</li>
                      <li>CID A37 = Coqueluche</li>
                      <li>CID A87 = Meningite Viral</li>
                      <li>CID B05 = Sarampo</li>
                      <li>CID B06 = Rubéola</li>
                      <li>Para mais informações, <a href="http://www.medicinanet.com.br/m/cid10.htm" target="_blank">clique aqui</a></li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1524245784377-1e77f3c853e5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=35a60739197ee7cdeef03f136388cb0c&auto=format&fit=crop&w=752&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Telefones Úteis</a></h6>
                <h6>Contatos</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <ol>
                      <li>Secretaria de Saúde Pelotas: (53) 3284-7700 </li>
                      <li>Secretaria de Saúde Canguçu: (53) 3252-3490</li>
                      <li>Laboratório Leivas Lang: (53) 3027-5522</li>
                      <li>SAMU: 193</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

          <!-- Example Social Card-->
          <div class="card mb-3">
            <a href="#">
              <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1533025782032-634283c7c7ad?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=3680a4fce212f1b6d97a171a8ea7527b&auto=format&fit=crop&w=1504&q=80" alt="">
            </a>
            <div class="card-body">
              <h6 class="card-title mb-1"><a href="#">Como Cadastrar um Convênio</a></h6>
              <h6>Tutorial de como cadastrar um convênio</h6>
            </div>
            <hr class="my-0">
            <div class="card-body small bg-faded">
              <div class="media">
                <div class="media-body">
                  <h6>Siga os passos abaixo:</h6>
                  <ol>
                    <li>Logue no sistema com seu usuário de acesso</li>
                    <li>Clique no menu lateral "Clínica"</li>
                    <li>Clique no submenu "Convênios"</li>
                    <li>Clique no botão "Novo Convênio"</li>
                    <li>Preencha com o nome do Convênio</li>
                    <li>Clique no botão "Salvar"</li>
                  </ol>  
                </div>
              </div>
            </div>
            <div class="card-footer small text-muted">by Healer</div>
          </div>

          <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1533026080863-c503c50220af?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=378d37d0a8694ddc8a854bc36a279079&auto=format&fit=crop&w=1460&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como Cadastrar um Plano</a></h6>
                <h6>Tutorial de como cadastrar um plano de saúde de um determinado convênio</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Clínica"</li>
                      <li>Clique no submenu "Planos"</li>
                      <li>Clique no botão "Novo Plano"</li>
                      <li>Preencha com o nome, valor de descontos e convênio do Plano</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1524245784377-1e77f3c853e5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=35a60739197ee7cdeef03f136388cb0c&auto=format&fit=crop&w=752&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como Cadastrar um Paciente</a></h6>
                <h6>Tutorial de como cadastrar um paciente</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Paciente"</li>
                      <li>Clique no botão "Novo Paciente"</li>
                      <li>Preencha com os dados do paciente</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1512428559087-560fa5ceab42?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=665f85219b6ad4ee4b274871593f3394&auto=format&fit=crop&w=1500&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como alterar a senha de um paciente</a></h6>
                <h6>Tutorial de como alterar a senha de acesso de um paciente</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Paciente"</li>
                      <li>Clique sobre o nome do paciente que deseja alterar a senha</li>
                      <li>Digite a nova senha no campo "Senha"</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1515377905703-c4788e51af15?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5f384fa6a6caa83e0ffdcef585e20c99&auto=format&fit=crop&w=1500&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como Cadastrar uma Especialidade</a></h6>
                <h6>Tutorial de como cadastrar uma especialidade atendida pela clínica</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Clínica"</li>
                      <li>Clique no submenu "Especialidades"</li>
                      <li>Clique no botão "Nova Especialidade"</li>
                      <li>Preencha com o nome da especialidade</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1512102438733-bfa4ed29aef7?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=256685797dc57cbf971c3a2fb0d1e0fb&auto=format&fit=crop&w=667&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como Cadastrar editar os dados da  Clínica</a></h6>
                <h6>Tutorial de como editar os dados da clínica</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Clínica"</li>
                      <li>Clique no submenu "Dados Geais"</li>
                      <li>Clique no botão "Editar Clínica"</li>
                      <li>Preencha com os dados da clínica</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>

            <!-- Example Social Card-->
          <div class="card mb-3">
              <a href="#">
                <img class="card-img-top img-fluid w-100" src="https://images.unsplash.com/photo-1512102438733-bfa4ed29aef7?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=256685797dc57cbf971c3a2fb0d1e0fb&auto=format&fit=crop&w=667&q=80" alt="">
              </a>
              <div class="card-body">
                <h6 class="card-title mb-1"><a href="#">Como Cadastrar editar os dados da  Clínica</a></h6>
                <h6>Tutorial de como editar os dados da clínica</h6>
              </div>
              <hr class="my-0">
              <div class="card-body small bg-faded">
                <div class="media">
                  <div class="media-body">
                    <h6>Siga os passos abaixo:</h6>
                    <ol>
                      <li>Logue no sistema com seu usuário de acesso</li>
                      <li>Clique no menu lateral "Clínica"</li>
                      <li>Clique no submenu "Dados Geais"</li>
                      <li>Clique no botão "Editar Clínica"</li>
                      <li>Preencha com os dados da clínica</li>
                      <li>Clique no botão "Salvar"</li>
                    </ol>  
                  </div>
                </div>
              </div>
              <div class="card-footer small text-muted">by Healer</div>
            </div>
            
            <!-- Fim dos Cards-->
        </div>
      </div>

@endsection