@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Financeiro</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('receitas.index') }}">Contas a Receber</a>
        </li>
        <li class="breadcrumb-item active">
            @if ($action == 1)
                Nova Receita
            @else
                Editar Receita
            @endif
        </li>
      </ol>


        @if ($action == 1)
            <form class="form-horizontal" action="{{ route('receitas.store') }}" method="POST">
            
        @else
            <form class="form" action="{{ route('receitas.update', $reg->id) }}" method="post">
            {!! method_field('put') !!}
            
        @endif
      
        {{ csrf_field() }}
        

        <div class="form-group">
            <label class="control-label col-sm-4" for="title">Título:</label>
            <div class="col-md-5">
                <input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ $reg->title or old('title') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="value">Valor:</label>
            <div class="col-md-5"> 
                <input type="tel" class="form-control" id="value" placeholder="Valor" name="value" value="{{ $reg->value or old('value') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="payDay">Data de Pagamento:</label>
            <div class="col-md-5"> 
                <input type="date" class="form-control" id="payDay" placeholder="Data de Pagamento" name="payDay" value="{{ $reg->payDay or old('payDay') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="description">Descrição:</label>
            <div class="col-md-5"> 
                <input type="text" class="form-control" id="description" placeholder="Descrição" name="description" value="{{ $reg->description or old('description') }}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="id_categories">Categoria:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_categories" name="id_categories">
                    <option value="0">Selecione</option>
                    @foreach ($categories as $c)
                        <option value="{{ $c->id }}" @if ((isset ($reg) and $c->id == $reg->id_categories) or old('id_categories') == $c->id) selected @endif>{{ $c->name }}</option>
                    @endforeach
                </select>   
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="id_employees">Funcionário:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_employees" name="id_employees">
                    <option value="0">Selecione</option>
                    @foreach ($employees as $e)
                        <option value="{{ $e->id }}" @if ((isset ($reg) and $e->id == $reg->id_employees) or old('id_employees') == $e->id) selected @endif>{{ $e->name }}</option>
                    @endforeach
                </select>   
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="id_agreements">Convênio:</label>
            <div class="col-md-5">
                <select class="form-control" id="id_agreements" name="id_agreements">
                    <option value="0">Selecione</option>
                    @foreach ($agreements as $a)
                        <option value="{{ $a->id }}" @if ((isset ($reg) and $a->id == $reg->id_agreements) or old('id_agreements') == $a->id) selected @endif>{{ $a->name }}</option>
                    @endforeach
                </select>   
            </div>
        </div>
        
        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Salvar</button>
            </div>
        </div>
                
    </form>

    @if ($action == 2)
        <form style="display: inline-block;" method="POST" action="{{ route('receitas.destroy', $reg->id ) }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            

            <button type="submit" class="btn btn-danger">Excluir</button>
        </form>
    @endif
@endsection