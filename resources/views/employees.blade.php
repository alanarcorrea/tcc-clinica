@extends('base')

@section('container')

<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Funcionários</a>
        </li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-circle"></i> Funcionários
        </div>
        <a href="{{ route('funcionarios.create') }}" class="btn btn-success">Novo Funcionário</a>

        @foreach ( $funcionarios as $f )
          <a class="list-group-item list-group-item-action" href="/funcionarios/{{$f->id}}/edit">
            <div class="media">
              <img class="d-flex mr-3 rounded-circle" src="/images/logo-square.png" alt="">
              <div class="media-body">
                <strong>{{ $f->name }}</strong>
                <div class="text-muted smaller">{{ $f->city }} - {{ $f->cellPhone }}</div>
              </div>
            </div>
          </a>
        @endforeach

          </div>
        </div>
      </div>

@endsection      