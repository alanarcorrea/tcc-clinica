<img src="images/logo-healer.png" class="img-rounded">

<h5 style="font-size: 24px; text-align: center; display: block;">Relatório de Atendimentos | Mensal</h5>

<table width="100%" cellspacing="0" style="font-size: 18px">
  <thead>
    <tr>
      <th>Mês</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($calls as $c)
      <tr>
        <td>{{ $c->mes }}</td>
        <td>{{ $c->total }}</td>
        </td>
      </tr>  
    @endforeach
  </tbody>
</table>

<hr>

<table width="100%" cellspacing="0" style="font-size: 18px">
  <thead>
    <tr>
      <th>Total  de Atendimentos</th>
    </tr>
  </thead>
  <tbody>
    @foreach($total as $t)
      <tr>
        <td>{{ $t->total }}</td>
      </tr>  
    @endforeach
  </tbody>
</table>

<span style="display: block; margin: 100px auto 50px; text-align: center;">Relatório emitido em: {{ date('d/m/Y') }}</span>