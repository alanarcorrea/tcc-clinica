# Healer - Sistema de Gestão para Clínica Médica

## Aluno
  * [Alana Ramires Correa](@alanarcorrea) 

## Heroku
  * [http://healer-app.herokuapp.com](http://healer-app.herokuapp.com/login)

## Instalação

1. Instalar o gerenciador de dependencias [Composer](https://getcomposer.org/download/)

1. Realizar o clone do projeto
    
    **SSH**
    ```git
    git clone git@gitlab.com:alanarcorrea/tcc-clinica.git
    ```

    **HTTPS**
    ```git
    git clone https://gitlab.com/alanarcorrea/tcc-clinica.git
    ```

1. Abrir o projeto na sua IDE de preferência
1. Executar no terminal o comando `composer install`
1. Renomear o arquivo **.env.example** para **.env**
1. Executar no terminal o comando `php artisan key:generate`
1. Executar no terminal o comando `php artisan migrate`
1. Executar no terminal o comando `php artisan db:seed`
1. Executar no terminal o comando `php artisan serve`
1. Concluídos os passos, você já está pronto para colaborar no projeto.

## Licença
Todos os direitos reservados. O autor permite apenas a visualização do código, sendo proibida qualquer utilização do mesmo, no todo ou em parte.