<?php

use Illuminate\Database\Seeder;

class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('specialities')->insert([
            'id' => 1,
            'name' => "Angiologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 2,
            'name' => "Cancerologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 3,
            'name' => "Cardiologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 4,
            'name' => "Dermatologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 5,
            'name' => "Endoscopia"
        ]);
        DB::table('specialities')->insert([
            'id' => 6,
            'name' => "Geriatria"
        ]);
        DB::table('specialities')->insert([
            'id' => 7,
            'name' => "Ginecologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 8,
            'name' => "Homeopatia"
        ]);
        DB::table('specialities')->insert([
            'id' => 9,
            'name' => "Nefrologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 10,
            'name' => "Neurologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 11,
            'name' => "Pediatria"
        ]);
        DB::table('specialities')->insert([
            'id' => 12,
            'name' => "Pneumologia"
        ]);
        DB::table('specialities')->insert([
            'id' => 13,
            'name' => "Enfermagem"
        ]);
    }
}
