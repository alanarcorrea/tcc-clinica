<?php

use Illuminate\Database\Seeder;

class ClinicalProceduresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clinical_procedures')->insert([
            'id' => 1,
            'title' => "Exame de Sangue",
            'value' => 120,
            'note' => 'Necessário jejum de 8 horas',
            'id_specialities' => 13
        ]);

        DB::table('clinical_procedures')->insert([
            'id' => 2,
            'title' => "Exame de Urina",
            'value' => 80,
            'note' => 'Primeira urina da manhã',
            'id_specialities' => 13
        ]);

        DB::table('clinical_procedures')->insert([
            'id' => 3,
            'title' => "Endoscopia",
            'value' => 240,
            'note' => 'Necessário jejum de 12 horas e ingestão de água liberada',
            'id_specialities' => 5
        ]);

        DB::table('clinical_procedures')->insert([
            'id' => 4,
            'title' => "Teste de HIV",
            'value' => 90,
            'note' => 'Necessário jejum de 8 horas',
            'id_specialities' => 5
        ]);

        DB::table('clinical_procedures')->insert([
            'id' => 5,
            'title' => "Raio X Seios da Face",
            'value' => 100,
            'note' => 'Não utilizar metais durante o procedimento',
            'id_specialities' => 5
        ]);

    }
}
