<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => "Alana",
            'email' => "alanarcorrea@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "2018-06-30",
            'rg' => "1113109126",
            'cpf' => '11144477735',
            'cellPhone' => '0912312312',
            'homePhone' => '3218213123',
            'type' => 1,
            'address' => 'Lorem ipsum',
            'city' => 'Pelotas',
            'cep' => '96030680',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => "Admin",
            'email' => "admin@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "2018-06-30",
            'rg' => "1113109126",
            'cpf' => '11144477735',
            'cellPhone' => '0912312312',
            'homePhone' => '3218213123',
            'type' => 1,
            'address' => 'Lorem ipsum',
            'city' => 'Pelotas',
            'cep' => '96030680',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'name' => "Clínica Pelotense",
            'email' => "clinica_pelotas@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "2018-06-30",
            'rg' => "1113109126",
            'cpf' => '11144477735',
            'cellPhone' => '0912312312',
            'homePhone' => '3218213123',
            'type' => 1,
            'address' => 'Av. Duque de Caxias, 1087',
            'city' => 'Pelotas',
            'cep' => '96010610',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'name' => "Marilene Prestes Vieira",
            'email' => "marivieira12@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "1878-04-20",
            'rg' => "6758391085",
            'cpf' => '86739374859',
            'cellPhone' => '53991875647',
            'homePhone' => '32257900',
            'type' => 1,
            'address' => 'Av. Fernando Osório, 177',
            'city' => 'Pelotas',
            'cep' => '96898678',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 5,
            'name' => "Paulo Machado Lopes",
            'email' => "paulo.lopes@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "1997-07-10",
            'rg' => "8748391093",
            'cpf' => '09483049200',
            'cellPhone' => '53991809999',
            'homePhone' => '32245676',
            'type' => 1,
            'address' => 'Av. Bento Gonçalves, 678',
            'city' => 'Pelotas',
            'cep' => '96070985',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 6,
            'name' => "Amélia Kreptow Richardt",
            'email' => "amelia.richardt@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "1990-03-09",
            'rg' => "7365829875",
            'cpf' => '95748393874',
            'cellPhone' => '53991907645',
            'homePhone' => '32237676',
            'type' => 1,
            'address' => 'Rua Oscar Schimidt, 233',
            'city' => 'Pelotas',
            'cep' => '96090876',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 7,
            'name' => "João Henrique Lima",
            'email' => "joao.lima@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "1988-06-06",
            'rg' => "1123849202",
            'cpf' => '98742020309',
            'cellPhone' => '53981135467',
            'homePhone' => '32259076',
            'type' => 1,
            'address' => 'Rua Gonçalves Chaves, 998',
            'city' => 'Pelotas',
            'cep' => '96070989',
            'photo' => 'empty'
        ]);

        DB::table('users')->insert([
            'id' => 8,
            'name' => "Luana Briao Mascarenhas",
            'email' => "luana.mascarenhas@gmail.com",
            'password' => bcrypt('secret'),
            'birthDate' => "1980-05-10",
            'rg' => "7683910386",
            'cpf' => '78494039989',
            'cellPhone' => '53991908756',
            'homePhone' => '32227876',
            'type' => 1,
            'address' => 'Rua Barroso, 83',
            'city' => 'Pelotas',
            'cep' => '96089832',
            'photo' => 'empty'
        ]);
    }
}
