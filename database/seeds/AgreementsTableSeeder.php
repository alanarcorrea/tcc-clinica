<?php

use Illuminate\Database\Seeder;

class AgreementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agreements')->insert([
            'id' => 1,
            'name' => "IPÊ"
        ]);

        DB::table('agreements')->insert([
            'id' => 2,
            'name' => "Unimed"
        ]);

        DB::table('agreements')->insert([
            'id' => 3,
            'name' => "Vitta Saúde"
        ]);
        DB::table('agreements')->insert([
            'id' => 4,
            'name' => "Qualicorp"
        ]);
        DB::table('agreements')->insert([
            'id' => 5,
            'name' => "Saúde Maior"
        ]);
        DB::table('agreements')->insert([
            'id' => 6,
            'name' => "Angelus Pax"
        ]);
        DB::table('agreements')->insert([
            'id' => 7,
            'name' => "Sul Clínica"
        ]);
    }
}
