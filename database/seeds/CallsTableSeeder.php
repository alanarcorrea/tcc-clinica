<?php

use Illuminate\Database\Seeder;

class CallsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calls')->insert([
            'id' => 1,
            'start' => "2004-05-23T14:25:10",
            'end' => "2004-05-23T14:25:10",
            'summary' => "Sumário",
            'anamnesis' => "Anamnesis",
            'height' => "1",
            'weight' => "1",
            'pressure' => "1",
            'temperature' => "1",
            'diagnosticHypothesis' => "diagnóstimo",
            'prescriptionMedication' => "prescrição",
            'prescriptionExam' => "exame",
            'attachment' => "anexo",
            'date' => "2004-05-23",
            'valueConsultation' => "12",
            'valueClinicalProcedure' => "12",
            'clinicalProcedure' => "12",
            'note' => "note",
            'id_pacients' => "1",
            'id_employees' => "4"
        ]);
    }
}
