<?php

use Illuminate\Database\Seeder;

class ClinicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clinics')->insert([
            'id' => 1,
            'socialName' => 'Clínica José de Aloisio',
            'fantasyName' => 'Clínica Pelotense',
            'cnpj' => 13977210000186,
            'bank' => 'Santander',
            'agency' => 988,
            'bankAccount' => 83123 ,
            'id_users' => 3

        ]);
    }
}
