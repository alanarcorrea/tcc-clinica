<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => 1,
            'name' => "Salário"
        ]);
        DB::table('categories')->insert([
            'id' => 2,
            'name' => "Aluguel"
        ]);
        DB::table('categories')->insert([
            'id' => 3,
            'name' => "Energia Elétrica"
        ]);
        DB::table('categories')->insert([
            'id' => 4,
            'name' => "Água"
        ]);
        DB::table('categories')->insert([
            'id' => 5,
            'name' => "Custos Convênio"
        ]);
    }
}
