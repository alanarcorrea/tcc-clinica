<?php

use Illuminate\Database\Seeder;

class PacientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pacients')->insert([
            'id' => 1,
            'recordDate' => "2018-06-07",
            'id_agreements' => 1,
            'id_plans' => 2,
            'id_users' => 4
        ]);

        DB::table('pacients')->insert([
            'id' => 2,
            'recordDate' => "2018-10-07",
            'id_agreements' => 2,
            'id_plans' => 4,
            'id_users' => 5
        ]);

        DB::table('pacients')->insert([
            'id' => 3,
            'recordDate' => "2018-11-04",
            'id_agreements' =>4,
            'id_plans' => 2,
            'id_users' => 6
        ]);

        DB::table('pacients')->insert([
            'id' => 4,
            'recordDate' => "2018-03-05",
            'id_agreements' => 3,
            'id_plans' => 4,
            'id_users' => 7
        ]);

        DB::table('pacients')->insert([
            'id' => 5,
            'recordDate' => "2018-09-09",
            'id_agreements' => 1,
            'id_plans' => 3,
            'id_users' => 8
        ]);
    }
}
