<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'id' => 1,
            'name' => 'Estudante',
            'discountConsultation' => 10,
            'discountProcedure' => 10,
            'id_agreements' => 1
        ]);
        DB::table('plans')->insert([
            'id' => 2,
            'name' => 'Família',
            'discountConsultation' => 70,
            'discountProcedure' => 60,
            'id_agreements' => 2
        ]);
        DB::table('plans')->insert([
            'id' => 3,
            'name' => 'Gold',
            'discountConsultation' => 30,
            'discountProcedure' => 30,
            'id_agreements' => 2
        ]);
        DB::table('plans')->insert([
            'id' => 4,
            'name' => 'Silver',
            'discountConsultation' => 20,
            'discountProcedure' => 12,
            'id_agreements' => 4
        ]);
    }
}
