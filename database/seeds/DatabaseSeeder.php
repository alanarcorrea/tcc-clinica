<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(AgreementsTableSeeder::class);
        $this->call(FunctionsTableSeeder::class);
        $this->call(SpecialitiesTableSeeder::class);
        $this->call(ClinicsTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(PacientsTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(ClinicalProceduresTableSeeder::class);
    }
}
