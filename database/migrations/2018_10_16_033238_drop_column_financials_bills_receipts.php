<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnFinancialsBillsReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills_receipts', function(Blueprint $table) {
            $table->dropColumn('recipient');
            $table->dropColumn('dueDate');
            $table->dropColumn('interest');
            $table->dropColumn('amount');
            $table->dropColumn('ourNumber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills_receipts', function(Blueprint $table) {
            $table->dropColumn('recipient');
            $table->dropColumn('dueDate');
            $table->dropColumn('interest');
            $table->dropColumn('amount');
            $table->dropColumn('ourNumber');
        });
    }
}
