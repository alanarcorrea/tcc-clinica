<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->string('summary')->nullable();
            $table->string('anamnesis');
            $table->float('height');
            $table->float('weight');
            $table->float('pressure');
            $table->float('temperature');
            $table->string('diagnosticHypothesis');
            $table->string('prescriptionMedication');
            $table->string('prescriptionExam');
            $table->string('attachment')->nullable();
            $table->date('date');
            $table->float('valueConsultation');
            $table->float('valueClinicalProcedure');
            $table->string('clinicalProcedure');
            $table->string('note');

            $table->integer('id_pacients')->unsigned();
            $table->foreign('id_pacients')->references('id')->on('pacients')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_employees')->unsigned();
            $table->foreign('id_employees')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
