<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSpecialtyIdHourSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->integer('id_specialities')->unsigned()->nullable();
            $table->foreign('id_specialities')->references('id')->on('specialities')->onUpdate('cascade')->onDelete('cascade');

            $table->string('hour')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('id_specialities');
            $table->dropColumn('hour');
        });
    }
}
