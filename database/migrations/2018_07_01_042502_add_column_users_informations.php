<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsersInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {            
            $table->date('birthDate')->nullable();
            $table->string('rg')->nullable();
            $table->string('cpf')->nullable();
            $table->string('cellPhone');
            $table->string('homePhone');
            $table->integer('type')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('cep');
            $table->string('photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('birthDate');
            $table->dropColumn('rg');
            $table->dropColumn('cpf');
            $table->dropColumn('cellPhone');
            $table->dropColumn('homePhone');
            $table->dropColumn('type');
            $table->dropColumn('address');
            $table->dropColumn('city');
            $table->dropColumn('cep');
            $table->dropColumn('photo');
        });
    }
}
