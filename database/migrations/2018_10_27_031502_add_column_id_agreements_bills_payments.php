<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIdAgreementsBillsPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills_payments', function (Blueprint $table) {
            $table->integer('id_agreements')->unsigned()->nullable();
            $table->foreign('id_agreements')->references('id')->on('agreements')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills_payments', function (Blueprint $table) {
            $table->dropColumn('id_agreements');
        });
    }
}
