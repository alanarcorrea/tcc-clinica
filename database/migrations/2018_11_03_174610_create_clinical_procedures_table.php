<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinical_procedures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->float('value');
            $table->string('note');

            $table->integer('id_specialities')->unsigned();
            $table->foreign('id_specialities')->references('id')->on('specialities')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinical_procedures');
    }
}
