<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->float('value');
            $table->string('recipient');
            $table->date('payDay');
            $table->date('dueDate');
            $table->float('interest');
            $table->float('amount');
            $table->string('ourNumber');
            $table->string('description');

            $table->integer('id_categories')->unsigned();
            $table->foreign('id_categories')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_employees')->unsigned();
            $table->foreign('id_employees')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_calls')->unsigned();
            $table->foreign('id_calls')->references('id')->on('calls')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills_receipts');
    }
}
