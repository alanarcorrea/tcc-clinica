<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');

            $table->integer('id_pacients')->unsigned();
            $table->foreign('id_pacients')->references('id')->on('pacients')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_employees')->unsigned();
            $table->foreign('id_employees')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
