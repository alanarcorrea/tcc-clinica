<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesHasSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_has_specialities', function (Blueprint $table) {
            $table->integer('id_employees')->unsigned();
            $table->foreign('id_employees')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('id_specialities')->unsigned();
            $table->foreign('id_specialities')->references('id')->on('specialities')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_has_specialities');
    }
}
