-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema healer
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema healer
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `healer` DEFAULT CHARACTER SET utf8 ;
USE `healer` ;

-- -----------------------------------------------------
-- Table `healer`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  `rg` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `foneCelular` VARCHAR(20) NOT NULL,
  `foneResidencial` VARCHAR(20) NOT NULL,
  `senha` VARCHAR(15) NOT NULL,
  `tipo` INT(1) NOT NULL,
  `endereco` VARCHAR(100) NOT NULL,
  `cidade` VARCHAR(40) NOT NULL,
  `cep` VARCHAR(9) NOT NULL,
  `foto` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`clinica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`clinica` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `razaoSocial` VARCHAR(100) NOT NULL,
  `nomeFantasia` VARCHAR(100) NOT NULL,
  `cnpj` VARCHAR(14) NOT NULL,
  `banco` VARCHAR(30) NOT NULL,
  `agencia` VARCHAR(30) NOT NULL,
  `contaBancaria` VARCHAR(15) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `endereco` VARCHAR(100) NOT NULL,
  `cidade` VARCHAR(40) NOT NULL,
  `cep` VARCHAR(9) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_clinica_usuario1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_clinica_usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `healer`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`convenio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`convenio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`paciente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`paciente` (
  `id` INT NOT NULL,
  `dataRegistro` DATE NOT NULL,
  `idUsuario` INT NOT NULL,
  `idConvenio` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_patient_usuario1_idx` (`idUsuario` ASC),
  INDEX `fk_patient_convenio1_idx` (`idConvenio` ASC),
  CONSTRAINT `fk_patient_usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `healer`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_convenio1`
    FOREIGN KEY (`idConvenio`)
    REFERENCES `healer`.`convenio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`plano`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`plano` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descontoConsulta` DOUBLE(8,2) NOT NULL,
  `descontoProcedimento` DOUBLE(8,2) NOT NULL,
  `idConvenio` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_plano_convenio1_idx` (`idConvenio` ASC),
  CONSTRAINT `fk_plano_convenio1`
    FOREIGN KEY (`idConvenio`)
    REFERENCES `healer`.`convenio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`tipo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`documento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(100) NOT NULL,
  `idTipo` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_documento_tipo1_idx` (`idTipo` ASC),
  CONSTRAINT `fk_documento_tipo1`
    FOREIGN KEY (`idTipo`)
    REFERENCES `healer`.`tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`funcao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`funcao` (
  `id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL COMMENT 'médico, secretária, enfermeiro',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`funcionario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ctps` INT(11) NOT NULL,
  `salario` DOUBLE(8,2) NOT NULL,
  `cnh` VARCHAR(11) NULL,
  `banco` VARCHAR(30) NOT NULL,
  `agencia` VARCHAR(30) NOT NULL,
  `contaBancaria` VARCHAR(15) NOT NULL,
  `crm` VARCHAR(15) NULL,
  `idFuncao` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_funcionario_funcao1_idx` (`idFuncao` ASC),
  INDEX `fk_funcionario_usuario1_idx` (`idUsuario` ASC),
  CONSTRAINT `fk_funcionario_funcao1`
    FOREIGN KEY (`idFuncao`)
    REFERENCES `healer`.`funcao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_funcionario_usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `healer`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`agenda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`agenda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `idFuncionario` INT NOT NULL,
  `paciente_id` INT NOT NULL,
  PRIMARY KEY (`id`, `idFuncionario`),
  INDEX `fk_agenda_funcionario1_idx` (`idFuncionario` ASC),
  INDEX `fk_agenda_paciente1_idx` (`paciente_id` ASC),
  CONSTRAINT `fk_agenda_funcionario1`
    FOREIGN KEY (`idFuncionario`)
    REFERENCES `healer`.`funcionario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_agenda_paciente1`
    FOREIGN KEY (`paciente_id`)
    REFERENCES `healer`.`paciente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`categoria` (
  `id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL COMMENT 'nome = \"Pagametos funcionários\" , \"Recebimentos de consultas\", \"Recebimento Procedimentos\", \"Pagamentos básicos da clínica (aluguel, água,luz)\".',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`contasPagar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`contasPagar` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(100) NOT NULL,
  `valor` DOUBLE(8,2) NOT NULL,
  `destinatario` VARCHAR(150) NOT NULL COMMENT 'transacao \"receita\" e \"despesa\". Texto já especificado no layout.',
  `dataPagamento` DATE NOT NULL,
  `dataVencimento` DATE NOT NULL,
  `juros` DOUBLE(8,2) NOT NULL,
  `valorTotal` DOUBLE(8,2) NOT NULL,
  `nossoNumero` VARCHAR(50) NOT NULL,
  `descricao` VARCHAR(150) NULL,
  `idCategoria` INT NOT NULL,
  `idFuncionario` INT NULL,
  PRIMARY KEY (`id`, `idFuncionario`),
  INDEX `fk_transacaoFinanceira_categoria1_idx` (`idCategoria` ASC),
  INDEX `fk_transacaoFinanceira_funcionario1_idx` (`idFuncionario` ASC),
  CONSTRAINT `fk_transacaoFinanceira_categoria1`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `healer`.`categoria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transacaoFinanceira_funcionario1`
    FOREIGN KEY (`idFuncionario`)
    REFERENCES `healer`.`funcionario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`especialidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`especialidade` (
  `id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`funcionario_has_especialidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`funcionario_has_especialidade` (
  `idFuncionario` INT NOT NULL,
  `idEspecialidade` INT NOT NULL,
  PRIMARY KEY (`idFuncionario`, `idEspecialidade`),
  INDEX `fk_funcionario_has_especialidade_especialidade1_idx` (`idEspecialidade` ASC),
  INDEX `fk_funcionario_has_especialidade_funcionario1_idx` (`idFuncionario` ASC),
  CONSTRAINT `fk_funcionario_has_especialidade_funcionario1`
    FOREIGN KEY (`idFuncionario`)
    REFERENCES `healer`.`funcionario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_funcionario_has_especialidade_especialidade1`
    FOREIGN KEY (`idEspecialidade`)
    REFERENCES `healer`.`especialidade` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`atendimento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`atendimento` (
  `id` INT NOT NULL,
  `inicio` DATETIME NOT NULL,
  `fim` DATETIME NOT NULL,
  `resumo` VARCHAR(300) NOT NULL,
  `anamnese` VARCHAR(600) NOT NULL,
  `altura` DOUBLE(8,2) NOT NULL,
  `peso` DOUBLE(8,2) NOT NULL,
  `pressao` DOUBLE(8,2) NOT NULL,
  `temperatura` DOUBLE(8,2) NOT NULL,
  `hipoteseDiagnostica` VARCHAR(200) NOT NULL,
  `prescricaoMedicamento` VARCHAR(300) NOT NULL,
  `prescricaoExame` VARCHAR(200) NOT NULL,
  `anexo` VARCHAR(45) NOT NULL,
  `data` DATE NOT NULL,
  `valorConsulta` DOUBLE(8,2) NOT NULL,
  `valorProcedimentoClinico` DOUBLE(8,2) NOT NULL,
  `procedimentoClinico` VARCHAR(200) NULL,
  `observacao` VARCHAR(150) NULL,
  `idPaciente` INT NOT NULL,
  `idFuncionario` INT NOT NULL,
  PRIMARY KEY (`id`, `idFuncionario`),
  INDEX `fk_atendimento_paciente1_idx` (`idPaciente` ASC),
  INDEX `fk_atendimento_funcionario1_idx` (`idFuncionario` ASC),
  CONSTRAINT `fk_atendimento_paciente1`
    FOREIGN KEY (`idPaciente`)
    REFERENCES `healer`.`paciente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_atendimento_funcionario1`
    FOREIGN KEY (`idFuncionario`)
    REFERENCES `healer`.`funcionario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `healer`.`contasReceber`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `healer`.`contasReceber` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(100) NOT NULL,
  `valor` DOUBLE(8,2) NOT NULL,
  `destinatario` VARCHAR(150) NOT NULL COMMENT 'transacao \"receita\" e \"despesa\". Texto já especificado no layout.',
  `dataPagamento` DATE NOT NULL,
  `dataVencimento` DATE NOT NULL,
  `juros` DOUBLE(8,2) NOT NULL,
  `valorTotal` DOUBLE(8,2) NOT NULL,
  `nossoNumero` VARCHAR(50) NOT NULL,
  `descricao` VARCHAR(150) NULL,
  `idAtendimento` INT NOT NULL,
  `idFuncionario` INT NOT NULL,
  `idCategoria` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transacaoFinanceira_categoria1_idx` (`idCategoria` ASC),
  INDEX `fk_contasReceber_atendimento1_idx` (`idAtendimento` ASC, `idFuncionario` ASC),
  CONSTRAINT `fk_transacaoFinanceira_categoria10`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `healer`.`categoria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contasReceber_atendimento1`
    FOREIGN KEY (`idAtendimento` , `idFuncionario`)
    REFERENCES `healer`.`atendimento` (`id` , `idFuncionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
